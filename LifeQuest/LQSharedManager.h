//
//  SharedManager.h
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface LQSharedManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (LQSharedManager *)sharedManager;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)initializeQuestLists;

@end
