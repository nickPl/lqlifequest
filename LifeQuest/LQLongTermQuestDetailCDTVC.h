//
//  LTDetailCDTVC.h
//  LifeQuest
//
//  Created by Plumb on 27.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class  LQLongTermLifeQuest;

@interface LQLongTermQuestDetailCDTVC : UIViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) LQLongTermLifeQuest *currentLongTermQuest;

@end
