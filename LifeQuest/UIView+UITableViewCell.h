//
//  UIView+UITableViewCell.h
//  LifeQuest
//
//  Created by Plumb on 13.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UITableViewCell)

- (UITableViewCell *)superCell;

@end
