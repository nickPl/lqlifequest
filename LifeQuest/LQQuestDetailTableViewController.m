//
//  LQQuestDetailTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 17.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQQuestDetailTableViewController.h"
#import "LQLifeQuest.h"
#import "LQSharedManager.h"
#import "LQLongTermLifeQuest.h"
#import "LQPositiveShortTermLifeQuest.h"
#import "LQNegativeShortTermLifeQuest.h"
#import "LQLifePlan.h"
#import "JGProgressHUD.h"
#import "JGProgressHUDSuccessIndicatorView.h"


@interface LQQuestDetailTableViewController ()

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (strong, nonatomic) NSIndexPath *selectedQuestCategory;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutletCollection(UITableViewCell) NSArray *categoriesCells;

@end

@implementation LQQuestDetailTableViewController

#pragma mark - Custom Accessors

- (void)setSelectedQuestCategory:(NSIndexPath *)selectedQuestCategory
{
  if (_selectedQuestCategory != selectedQuestCategory)
  {
    _selectedQuestCategory = selectedQuestCategory;
    
    switch (_selectedQuestCategory.row)
    {
      case 0:
        self.questClass = NSStringFromClass([LQLongTermLifeQuest class]);
        break;
      case 1:
        self.questClass = NSStringFromClass([LQPositiveShortTermLifeQuest class]);
        break;
      case 2:
        self.questClass = NSStringFromClass([LQNegativeShortTermLifeQuest class]);
        break;
        
      default:
        break;
    }
  }
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  for (UITableViewCell *cell in self.categoriesCells)
  {
    cell.selectedBackgroundView = selectionView;
  }
  
  self.descriptionTextView.text = @"";
  self.noteTextView.text = @"";
  [self.titleTextField becomeFirstResponder];
  
  if (self.lifePlan)
  {
    self.selectedQuestCategory = [NSIndexPath indexPathForRow:0 inSection:3];
  }
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender
{
  [self closeScreen];
}

- (IBAction)saveAction:(id)sender
{
  Class questClass = NSClassFromString(self.questClass);
  
  if ([questClass isSubclassOfClass:[LQLifeQuest class]])
  {
    LQLifeQuest *quest = [questClass insertInManagedObjectContext:[[LQSharedManager sharedManager]managedObjectContext]];
    quest.title = self.titleTextField.text;
    quest.questDescription = self.descriptionTextView.text;
    quest.note = self.noteTextView.text;
    
    if (self.lifePlan)
    {
      [self.lifePlan addNewQuest:quest];
    }
    
    [[LQSharedManager sharedManager]saveContext];
  }
  
  JGProgressHUD *hud = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
  hud.indicatorView = [[JGProgressHUDSuccessIndicatorView alloc]init];
  hud.textLabel.text = @"Added!";
  
  [hud showInView:self.view];
  
  [self performSelector:@selector(closeScreen) withObject:nil afterDelay:0.6];
}

- (void)closeScreen
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return self.lifePlan ? 4 : 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return section == 3 ? 3 : 1;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  if (indexPath.section == 3)
  {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
    {
      return;
    }
    else
    {
      cell.accessoryType = UITableViewCellAccessoryCheckmark;
      
      UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:self.selectedQuestCategory];
      oldCell.accessoryType = UITableViewCellAccessoryNone;
    
      self.selectedQuestCategory = indexPath;
    }
  }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
  UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
  header.contentView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
  header.textLabel.textColor = [UIColor colorWithRed:0.43 green:0.43 blue:0.45 alpha:1];
  header.textLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:14];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
  
  self.saveButton.enabled = ([newText length] > 0);
  
  return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if ([textField isEqual:self.titleTextField])
  {
    [textField resignFirstResponder];
  }
  
  return NO;
}

@end
