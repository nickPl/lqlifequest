//
//  LQNegativeShortTermQuestsCDTVC.h
//  LifeQuest
//
//  Created by Plumb on 16.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQCoreDataTableViewController.h"

@interface LQNegativeShortTermQuestsCDTVC : LQCoreDataTableViewController

@property (strong, nonatomic) NSArray *allQuestsArray;

@end
