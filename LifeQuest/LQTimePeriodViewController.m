//
//  ViewController.m
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQTimePeriodViewController.h"
#import "LQLongTermQuestsCDTVC.h"

@interface LQTimePeriodViewController ()

@property (assign, nonatomic) NSInteger numberOfWeeks;

@end

@implementation LQTimePeriodViewController

#pragma mark - Lifecycle

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
  return nil;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (IBAction)selectPeriodAction:(UIButton *)sender
{
  self.numberOfWeeks = sender.tag;
  
  [[NSUserDefaults standardUserDefaults]setInteger:sender.tag forKey:@"NumberOfQuests"];
  NSLog(@"NumberOfQuests: %ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"]);
  
  [self performSegueWithIdentifier:@"addLongTermQuestsSegue" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addLongTermQuestsSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQLongTermQuestsCDTVC *cdtvc = (LQLongTermQuestsCDTVC *)nc.topViewController;
    cdtvc.numberOfWeeks = self.numberOfWeeks;
  }
}

@end
