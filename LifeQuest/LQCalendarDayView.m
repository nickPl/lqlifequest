//
//  LQCalendarDayView.m
//  LifeQuest
//
//  Created by Plumb on 15.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQCalendarDayView.h"

typedef enum
{
  LQCalendarDayViewTypeToday,
  LQCalendarDayViewTypeFuture,
  LQCalendarDayViewTypePast,
  
} LQCalendarDayViewType;

@interface LQCalendarDayView ()

@property (assign, nonatomic) NSUInteger todayWeekDay;
@property (assign, nonatomic) LQCalendarDayViewType type;

@end

@implementation LQCalendarDayView

#pragma mark - Lifecycle

- (instancetype)initFithFrame:(CGRect)frame andIndexNumber:(NSUInteger)theIndexNumber
{
  self.todayWeekDay = [self todayDayNumber];
  
  LQCalendarDayView *dayView = [[LQCalendarDayView alloc]initWithFrame:frame];
  dayView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  [dayView.layer setBorderColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1].CGColor];
  [dayView.layer setBorderWidth:2.0];
  
  //----------------//
  
  UILabel *dayLabel = [[UILabel alloc]init];
  
  [dayLabel setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:13]];
  [dayLabel setTextAlignment:NSTextAlignmentCenter];
  [dayLabel setBackgroundColor:[UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:1]]; 
  [dayLabel setFrame:CGRectMake(5, 5, dayView.frame.size.width - 10, (dayView.frame.size.height * 0.85) - 10)];
  [dayLabel setTextColor:[UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1]];
  
  dayLabel.lineBreakMode = NSLineBreakByWordWrapping;
  dayLabel.numberOfLines = 0;
  dayLabel.text = [self dateForWeekDay:theIndexNumber];
  
  //----------------//
  
  UIView *checkView = [[UIView alloc]initWithFrame:CGRectMake(5, dayView.frame.size.height - 4, dayView.frame.size.width - 10, 2)];
  
  UIColor *checkColor = nil;
  
  switch (self.type)
  {
    case LQCalendarDayViewTypeToday:
      checkColor = [UIColor colorWithRed:0 green:0.59 blue:0.35 alpha:1];
      break;
    case LQCalendarDayViewTypeFuture:
      checkColor = [UIColor colorWithRed:0.31 green:0.73 blue:0.95 alpha:1];
      break;
    case LQCalendarDayViewTypePast:
      checkColor = [UIColor colorWithRed:1 green:0.75 blue:0.25 alpha:1];
      break;
      
    default:
      break;
  }
  checkView.backgroundColor = checkColor;
  
  [dayView addSubview:checkView];
  [dayView addSubview:dayLabel];
  
  return dayView;
}

#pragma mark - Date Methods

- (NSUInteger)todayDayNumber
{
  NSCalendar *calendar = [NSCalendar currentCalendar];
  
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitWeekday
                                           fromDate:[NSDate date]];
  return [dateComp weekday];
}

- (NSString *)formatDate:(NSDate *)theDate
{
  static NSDateFormatter *formatter = nil;
  if (!formatter) {
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"EEE \n dd"];
  }
  return [formatter stringFromDate:theDate];
}

- (NSString *)dateForWeekDay:(NSUInteger)weekDay
{
  NSDate *weekDayDate = [NSDate date];
  
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components: NSCalendarUnitYear | NSCalendarUnitWeekOfYear
                                           fromDate:weekDayDate];
  [dateComp setWeekday:weekDay + 1];
  
  if (self.todayWeekDay == weekDay + 1)
  {
    self.type = LQCalendarDayViewTypeToday;
  }
  else if (self.todayWeekDay > weekDay + 1)
  {
    self.type = LQCalendarDayViewTypePast;
  }
  else
  {
    self.type = LQCalendarDayViewTypeFuture;
  }
  
  return [self formatDate:[calendar dateFromComponents:dateComp]];
}

@end
