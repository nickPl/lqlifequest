//
//  AppDelegate.m
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQAppDelegate.h"
#import "LQSharedManager.h"
#import "LQLifePlan.h"
#import "LQWeeklyQuestViewController.h"

@interface LQAppDelegate ()

@property (strong, nonatomic) LQLifePlan *lifePlan;

@end

@implementation LQAppDelegate

#pragma mark - Lifecycle

- (void)customizeAppearance
{  
  [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:1]];
  [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSFontAttributeName            : [UIFont fontWithName:@"AvenirNext-DemiBold" size:20]}];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //-----------------------------------------//
  
  [self customizeAppearance];
  
  //-----------------------------------------//
  
  [self registerDefaults];
  
  //-----------------------------------------//
  
  if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
  {
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [application registerUserNotificationSettings:mySettings];
  }
  
  //-----------------------------------------//
  
  [self handleFirstTime];
  
  //-----------------------------------------//
  
  if (launchOptions[UIApplicationLaunchOptionsLocalNotificationKey] != nil)
  {
    UILocalNotification *notification = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    [self application:application didReceiveLocalNotification:notification];
  }
  
  //-----------------------------------------//
  
  return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
  application.applicationIconBadgeNumber = 0;
  
  if (!self.lifePlan)
  {
    self.lifePlan = [self fetchUserPlan];
  }
  
  NSString *notificationKey = [notification.userInfo objectForKey:@"NotificationKey"];
  
  if ([notificationKey isEqualToString:@"Weekly"])
  {
    [self.lifePlan handlingWeeklyLocalNotification:notification];
  }
}

- (void)applicationWillResignActive:(UIApplication *)application { }

- (void)applicationDidEnterBackground:(UIApplication *)application { }

- (void)applicationWillEnterForeground:(UIApplication *)application { }

- (void)applicationDidBecomeActive:(UIApplication *)application { }

- (void)applicationWillTerminate:(UIApplication *)application
{
  [[LQSharedManager sharedManager] saveContext];
}

#pragma mark - Fetch

- (LQLifePlan *)fetchUserPlan
{
  NSFetchRequest *request = [[NSFetchRequest alloc]init];
  
  NSEntityDescription *description = [NSEntityDescription entityForName:[LQLifePlan entityName]
                                                 inManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];
  [request setEntity:description];
  
  NSError *error = nil;
  NSArray *resultArray = [[[LQSharedManager sharedManager]managedObjectContext] executeFetchRequest:request
                                                                                              error:&error];
  if (error)
  {
    NSLog(@"%@", [error localizedDescription]);
  }
  
  if ([resultArray count] == 1)
  {
    return [resultArray firstObject];
  }
  else
  {
    return nil;
  }
}

#pragma mark - Handle First Time

- (void)handleFirstTime
{
  if ([[NSUserDefaults standardUserDefaults]boolForKey:@"FirstTime"])
  {
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"InitializeQuestLists"])
    {
      [[LQSharedManager sharedManager]initializeQuestLists];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"timePeriodViewController"];
    self.window.rootViewController = controller;
  }
  else if (![[NSUserDefaults standardUserDefaults]boolForKey:@"FirstTime"])
  {
    self.lifePlan = [self fetchUserPlan];
    
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"] > 1)
    {
      [self.lifePlan generateWeeklyQuest];
    }
    else if ([[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"] == 1)
    {
      [self.lifePlan generateWeeklyQuest];
      
      UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Last Quest"
                                                     message:@"It's your last quest. If you want continue, please add new one to your plan."
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
      [alert show];
    }
    else
    {
      self.lifePlan.currentWeeklyQuest = nil;
      self.lifePlan.currentLongTermQuests = nil;
      
      UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You period has ended"
                                                     message:@"Do you want to start new one now?"
                                                    delegate:self
                                           cancelButtonTitle:@"Ignore"
                                           otherButtonTitles:@"Start", nil];
      [alert show];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [storyboard instantiateViewControllerWithIdentifier:@"weeklyQuestNavigationViewController"];
    LQWeeklyQuestViewController *controller = (LQWeeklyQuestViewController *)nc.topViewController;
    controller.lifePlan = self.lifePlan;
    self.window.rootViewController = nc;
  }
}

#pragma mark - NSUserDefaults 

- (void)registerDefaults
{
  NSDictionary *dictionary = @{@"FirstTime"            : @YES,
                               @"InitializeQuestLists" : @NO};
  [[NSUserDefaults standardUserDefaults]registerDefaults:dictionary];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
  
  if ([buttonTitle isEqualToString:@"Start"])
  {
    [[LQSharedManager sharedManager].managedObjectContext deleteObject:self.lifePlan];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"FirstTime"];
    
    [self handleFirstTime];
  }
}

@end
