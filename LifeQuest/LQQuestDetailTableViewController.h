//
//  LQQuestDetailTableViewController.h
//  LifeQuest
//
//  Created by Plumb on 17.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLifePlan;

@interface LQQuestDetailTableViewController : UITableViewController

@property (copy, nonatomic) NSString *questClass;
@property (assign, nonatomic) LQLifePlan *lifePlan;

@end
