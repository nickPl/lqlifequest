//
//  AppDelegate.h
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface LQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

