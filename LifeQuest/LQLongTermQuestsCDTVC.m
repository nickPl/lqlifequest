//
//  LQLongTermQuestsCDTVC.m
//  LifeQuest
//
//  Created by Plumb on 16.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQLongTermQuestsCDTVC.h"
#import "LQLongTermLifeQuest.h"
#import "LQQuestDetailTableViewController.h"
#import "LQPositiveShortTermQuestsCDTVC.h"

@interface LQLongTermQuestsCDTVC ()

@end

@implementation LQLongTermQuestsCDTVC

@synthesize fetchedResultsController = _fetchedResultsController;

#pragma mark - Custome Accessors

- (NSFetchedResultsController *)fetchedResultsController
{
  if (_fetchedResultsController == nil)
  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([LQLongTermLifeQuest class])
                                                         inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    
    NSSortDescriptor *titleDescriptor = [[NSSortDescriptor alloc]initWithKey:@"title" ascending:YES];
    [fetchRequest setSortDescriptors:@[titleDescriptor]];
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:self.managedObjectContext
                                                                     sectionNameKeyPath:nil
                                                                              cacheName:nil];
    _fetchedResultsController.delegate = self;
  }
  return  _fetchedResultsController;
}

#pragma mark - Public

- (NSInteger)numberOfQuestsToSelect
{
  return self.numberOfWeeks / 12;
}

#pragma mark - Action



- (void)nextAction
{
  [self performSegueWithIdentifier:@"addPositiveQuestsSegue" sender:nil];
}

- (void)backAction
{
  [[self presentingViewController]dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addNewQuestSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQQuestDetailTableViewController *tvc = (LQQuestDetailTableViewController *)nc.topViewController;
    tvc.questClass = NSStringFromClass([LQLongTermLifeQuest class]);
  }
  else if ([segue.identifier isEqualToString:@"addPositiveQuestsSegue"])
  {
    LQPositiveShortTermQuestsCDTVC *cdtvc = (LQPositiveShortTermQuestsCDTVC *)segue.destinationViewController;
    cdtvc.numberOfWeeks = self.numberOfWeeks;
    cdtvc.allQuestsArray = [NSArray arrayWithArray:self.selectedQuestsArray];
  }
}

@end
