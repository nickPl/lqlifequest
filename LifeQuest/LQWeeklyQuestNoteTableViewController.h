//
//  LQWeeklyQuestNoteTableViewController.h
//  LifeQuest
//
//  Created by Plumb on 23.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLifeQuest;

@interface LQWeeklyQuestNoteTableViewController : UITableViewController

@property (assign, nonatomic) LQLifeQuest *quest;

@end
