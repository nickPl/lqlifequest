//
//  LQPositiveShortTermQuestsCDTVC.m
//  LifeQuest
//
//  Created by Plumb on 16.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQPositiveShortTermQuestsCDTVC.h"
#import "LQPositiveShortTermLifeQuest.h"
#import "LQQuestDetailTableViewController.h"
#import "LQNegativeShortTermQuestsCDTVC.h"

@interface LQPositiveShortTermQuestsCDTVC ()

@end

@implementation LQPositiveShortTermQuestsCDTVC

@synthesize fetchedResultsController = _fetchedResultsController;

#pragma mark - Custome Accessors

- (NSFetchedResultsController *)fetchedResultsController
{
  if (_fetchedResultsController == nil)
  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([LQPositiveShortTermLifeQuest class])
                                                         inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    
    NSSortDescriptor *titleDescriptor = [[NSSortDescriptor alloc]initWithKey:@"title" ascending:YES];
    [fetchRequest setSortDescriptors:@[titleDescriptor]];
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:self.managedObjectContext
                                                                     sectionNameKeyPath:nil
                                                                              cacheName:nil];
    _fetchedResultsController.delegate = self;
  }
  return  _fetchedResultsController;
}

#pragma mark - Public

- (NSInteger)numberOfQuestsToSelect
{
  switch (self.numberOfWeeks)
  {
    case 12:
      return 10;
      break;
    case 24:
      return 20;
      break;
    case 48:
      return 40;
      break;
      
    default:
      break;
  }
  return 0;
}

#pragma mark - Action

- (void)nextAction
{
  [self performSegueWithIdentifier:@"addNegativeQuestsSegue" sender:nil];
}

- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addNewQuestSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQQuestDetailTableViewController *tvc = (LQQuestDetailTableViewController *)nc.topViewController;
    tvc.questClass = NSStringFromClass([LQPositiveShortTermLifeQuest class]);
  }
  else if ([segue.identifier isEqualToString:@"addNegativeQuestsSegue"])
  {
    LQNegativeShortTermQuestsCDTVC *cdtvc = (LQNegativeShortTermQuestsCDTVC *)segue.destinationViewController;
    cdtvc.numberOfWeeks = self.numberOfWeeks;
    cdtvc.allQuestsArray = [self.allQuestsArray arrayByAddingObjectsFromArray:self.selectedQuestsArray];
  }
}

@end
