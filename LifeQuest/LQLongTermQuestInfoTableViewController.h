//
//  LTInfoViewController.h
//  LifeQuest
//
//  Created by Plumb on 28.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLongTermLifeQuest;

@interface LQLongTermQuestInfoTableViewController : UITableViewController

@property (strong, nonatomic) LQLongTermLifeQuest *currentLongTermQuest;

@end
