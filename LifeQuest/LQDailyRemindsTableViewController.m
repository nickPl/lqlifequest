//
//  LQDailyRemindsTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQDailyRemindsTableViewController.h"
#import "LQWeekDayRemind.h"
#import "LQSharedManager.h"

@interface LQDailyRemindsTableViewController ()

@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *weekDaysButtons;
@property (weak, nonatomic) IBOutlet UIImageView *selectAllImage;
@property (weak, nonatomic) IBOutlet UITableViewCell *selectAllCell;

@end

@implementation LQDailyRemindsTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  self.selectAllCell.selectedBackgroundView = selectionView;
  
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back Arrow"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(backAction)];
  backButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
  self.navigationItem.leftBarButtonItem = backButton;
  self.navigationItem.title = @"Daily Reminds";
  
  [self customizeUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  [self performSegueWithIdentifier:@"dailyRemindsUnwindSegue" sender:nil];
}

#pragma mark - Action

- (IBAction)selectDayAction:(UIButton *)sender
{
  if ([sender.backgroundColor isEqual:self.view.tintColor])
  {
    sender.backgroundColor = [UIColor clearColor];
    [sender setTitleColor:self.view.tintColor forState:UIControlStateNormal];
  
    LQWeekDayRemind *remind = [self.weekDaysArray objectAtIndex:[self.weekDaysButtons indexOfObject:sender]];
    [remind  setShouldRemindValue:NO];
  
    [[LQSharedManager sharedManager]saveContext];
  }
  else
  {
    sender.backgroundColor = self.view.tintColor;
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    LQWeekDayRemind *remind = [self.weekDaysArray objectAtIndex:[self.weekDaysButtons indexOfObject:sender]];
    [remind  setShouldRemindValue:YES];
    
    [[LQSharedManager sharedManager]saveContext];
  }
  self.selectAllImage.image = [self imageForSelectAllImageView];
}

- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI

- (void)customizeUI
{
  for (LQWeekDayRemind *remind in self.weekDaysArray)
  {
    UIButton *button = [self.weekDaysButtons objectAtIndex:[self.weekDaysArray indexOfObject:remind]];
    
    if (remind.shouldRemindValue)
    {
      button.backgroundColor = self.view.tintColor;
      [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
      button.backgroundColor = [UIColor clearColor];
      [button setTitleColor:self.view.tintColor forState:UIControlStateNormal];
    }
  }
  self.selectAllImage.image = [self imageForSelectAllImageView];
  LQWeekDayRemind *remind = [self.weekDaysArray lastObject];
  self.timePicker.date = remind.fireTime;
}

#pragma mark - Private

- (UIImage *)imageForSelectAllImageView
{
  return [self isAnyUnselectedDay] ? [UIImage imageNamed:@"SelectAllImg"] : [UIImage imageNamed:@"DeselectAllImg"];
}

- (BOOL)isAnyUnselectedDay
{
  BOOL count = NO;
  
  for (LQWeekDayRemind *remind in self.weekDaysArray)
  {
    if (!remind.shouldRemindValue)
    {
      count = YES;
      return count;
    }
  }
  return NO;
}

- (void)toggleAllAction
{
  if ([self isAnyUnselectedDay])
  {
    for (UIButton *dayButton in self.weekDaysButtons)
    {
      if ([dayButton.backgroundColor isEqual:[UIColor clearColor]])
      {
        dayButton.backgroundColor = self.view.tintColor;
        [dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        LQWeekDayRemind *remind = [self.weekDaysArray objectAtIndex:[self.weekDaysButtons indexOfObject:dayButton]];
        [remind  setShouldRemindValue:YES];
    
        [[LQSharedManager sharedManager]saveContext];
      }
    }
  }
  else
  {
    for (UIButton *dayButton in self.weekDaysButtons)
    {
      dayButton.backgroundColor = [UIColor clearColor];
      [dayButton setTitleColor:self.view.tintColor forState:UIControlStateNormal];
      
      LQWeekDayRemind *remind = [self.weekDaysArray objectAtIndex:[self.weekDaysButtons indexOfObject:dayButton]];
      [remind  setShouldRemindValue:NO];
      
      [[LQSharedManager sharedManager]saveContext];
    }
  }
  
  self.selectAllImage.image = [self imageForSelectAllImageView];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.section == 1)
  {
    return indexPath;
  }
  else
  {
    return nil;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  if (indexPath.section == 1)
  {
    [self toggleAllAction];
  }
}

#pragma mark - Navigation

- (void)createFireTime
{
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute
                                           fromDate:self.timePicker.date];
  NSDate *date = [calendar dateFromComponents:dateComp];
  
  for (LQWeekDayRemind *remind in self.weekDaysArray)
  {
    remind.fireTime = date;
    [[LQSharedManager sharedManager] saveContext];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"dailyRemindsUnwindSegue"])
  {
    [self createFireTime];
  }
}

@end
