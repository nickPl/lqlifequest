//
//  LTInfoViewController.m
//  LifeQuest
//
//  Created by Plumb on 28.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQLongTermQuestInfoTableViewController.h"
#import "LQLongTermLifeQuest.h"
#import "LQSharedManager.h"

@interface LQLongTermQuestInfoTableViewController ()

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

@end

@implementation LQLongTermQuestInfoTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.descriptionTextView.text = self.currentLongTermQuest.questDescription;
  self.noteTextView.text = self.currentLongTermQuest.note;
  
  [self.descriptionTextView becomeFirstResponder];
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender
{
  if (![self.currentLongTermQuest.questDescription isEqualToString:self.descriptionTextView.text])
  {
    self.currentLongTermQuest.questDescription = self.descriptionTextView.text;
  }
  
  if (![self.currentLongTermQuest.note isEqualToString:self.noteTextView.text])
  {
    self.currentLongTermQuest.note = self.noteTextView.text;
  }
  
  [[LQSharedManager sharedManager] saveContext];
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
