//
//  UIView+UITableViewCell.m
//  LifeQuest
//
//  Created by Plumb on 13.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "UIView+UITableViewCell.h"

@implementation UIView (UITableViewCell)

- (UITableViewCell *)superCell
{
  if (!self.superview)
  {
    return nil;
  }
  
  if ([self.superview isKindOfClass:[UITableViewCell class]])
  {
    return (UITableViewCell *)self.superview;
  }
  
  return [self.superview superCell];
}

@end
