//
//  LQCalendarDayView.h
//  LifeQuest
//
//  Created by Plumb on 15.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LQCalendarDayView : UIView

- (instancetype)initFithFrame:(CGRect)frame andIndexNumber:(NSUInteger)theIndexNumber;

@end
