//
//  LTDetailCDTVC.m
//  LifeQuest
//
//  Created by Plumb on 27.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQLongTermQuestDetailCDTVC.h"
#import "LQSharedManager.h"
#import "LQStep.h"
#import "LQStepDetailViewController.h"
#import "LQLongTermLifeQuest.h"
#import "LQLongTermQuestInfoTableViewController.h"
#import "UIView+UITableViewCell.h"
#import <MRProgress/MRProgress.h>

@interface LQLongTermQuestDetailCDTVC ()

@property (weak, nonatomic) IBOutlet MRCircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LQLongTermQuestDetailCDTVC

#pragma mark - Custome Accessors

- (NSManagedObjectContext*)managedObjectContext
{
  if (!_managedObjectContext)
  {
    _managedObjectContext = [[LQSharedManager sharedManager] managedObjectContext];
  }
  return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController
{
  if (_fetchedResultsController == nil)
  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([LQStep class])
                                                         inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc]initWithKey:@"creationDate" ascending:YES];
    [fetchRequest setSortDescriptors:@[dateDescriptor]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"quest == %@", self.currentLongTermQuest];
    [fetchRequest setPredicate:predicate];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:self.managedObjectContext
                                                                     sectionNameKeyPath:nil
                                                                              cacheName:nil];
    _fetchedResultsController.delegate = self;
  }
  return  _fetchedResultsController;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.title = self.currentLongTermQuest.title;
  
  [self registerAsObserver];
  [self performFetch];
  
  self.progressView.borderWidth = 2;
  self.progressView.lineWidth = 4;
  self.progressView.animationDuration = 0.8;
  
  float progress = self.currentLongTermQuest.progressValue == 0 ? 0 : (float)self.currentLongTermQuest.progressValue / 100;
  [self.progressView setProgress:progress animated:NO];
}

- (void)dealloc
{
  _fetchedResultsController.delegate = nil;
  [self unregisterForChangeNotification];
}

#pragma mark - Observing

- (void)registerAsObserver
{
  [self.currentLongTermQuest addObserver:self
                              forKeyPath:@"questDescription"
                                 options:NSKeyValueObservingOptionInitial
                                 context:NULL];
  [self.currentLongTermQuest addObserver:self
                              forKeyPath:@"note"
                                 options:NSKeyValueObservingOptionInitial
                                 context:NULL];
  [self.currentLongTermQuest addObserver:self
                              forKeyPath:@"progress"
                                 options:NSKeyValueObservingOptionInitial
                                 context:NULL];
}

- (void)unregisterForChangeNotification
{
  [self.currentLongTermQuest removeObserver:self forKeyPath:@"questDescription"];
  [self.currentLongTermQuest removeObserver:self forKeyPath:@"note"];
  [self.currentLongTermQuest removeObserver:self forKeyPath:@"progress"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"questDescription"])
  {
    NSIndexPath *indexPathDescriptionRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPathDescriptionRow] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
  }
  else if ([keyPath isEqualToString:@"note"])
  {
    NSIndexPath *indexPathNoteRow = [NSIndexPath indexPathForRow:1 inSection:0];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPathNoteRow] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
  }
  else if ([keyPath isEqualToString:@"progress"])
  {
    float progress = self.currentLongTermQuest.progressValue == 0 ? 0 : (float)self.currentLongTermQuest.progressValue / 100;
    [self.progressView setProgress:progress animated:YES];
    if (self.currentLongTermQuest.progressValue == 100)
    {
      [self showCongratulationsMessage];
    }
  }
}

- (void)showCongratulationsMessage
{
  
  UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Congratulations"
                                                 message:@"Congratulations on completing your quest"
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
  alert.backgroundColor = [UIColor blackColor];
  [alert show];
}

#pragma mark - Action

- (IBAction)checkAction:(UIButton *)sender
{
  CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
  crossFade.duration = 0.3;
  crossFade.fromValue = nil;
  crossFade.toValue = nil;
  crossFade.removedOnCompletion = NO;
  crossFade.fillMode = kCAFillModeForwards;
  
  if ([sender.imageView.image isEqual:[UIImage imageNamed:@"UncheckGreen"]])
  {
    crossFade.fromValue = (id)[UIImage imageNamed:@"UncheckGreen"].CGImage;
    crossFade.toValue = (id)[UIImage imageNamed:@"CheckGreen"].CGImage;
    
    [sender.imageView.layer addAnimation:crossFade forKey:@"animateContenys"];
    [sender setImage:[UIImage imageNamed:@"CheckGreen"] forState:UIControlStateNormal];
  }
  else
  {
    crossFade.fromValue = (id)[UIImage imageNamed:@"CheckGreen"].CGImage;
    crossFade.toValue = (id)[UIImage imageNamed:@"UncheckGreen"].CGImage;
    
    [sender.imageView.layer addAnimation:crossFade forKey:@"animateContenys"];
    [sender setImage:[UIImage imageNamed:@"UncheckGreen"] forState:UIControlStateNormal];
  }
  
  UITableViewCell *cell = [sender superCell];
  
  if (cell)
  {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSIndexPath *indexPathForStepObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    
    LQStep *step = [self.fetchedResultsController objectAtIndexPath:indexPathForStepObject];
    
    [step toggleChecked];
    [self.currentLongTermQuest countProgress];
    
    [[LQSharedManager sharedManager] saveContext];
  }
}

- (IBAction)cancelAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - FetchedResultsController

- (void)performFetch
{
  NSError *error;
  if (![self.fetchedResultsController performFetch:&error])
  {
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    return;
  }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return section == 0 ? 3 : [self.fetchedResultsController.fetchedObjects count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell * cell = nil;

  if (indexPath.section == 0)
  {
    if (indexPath.row == 0)
    {

      cell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
      UILabel *descriptionLabel = (UILabel *)[cell viewWithTag:1002];
      
      descriptionLabel.text = self.currentLongTermQuest.questDescription ? self.currentLongTermQuest.questDescription :
      @"No description";
      
    }
    else if (indexPath.row == 1)
    {
      cell = [tableView dequeueReusableCellWithIdentifier:@"noteCell"];
      UILabel *noteTextLabel = (UILabel *)[cell viewWithTag:1003];
      
      noteTextLabel.text = self.currentLongTermQuest.note ? self.currentLongTermQuest.note : @"No note";
    }
    else if (indexPath.row == 2)
    {
     cell = [tableView dequeueReusableCellWithIdentifier:@"addNoteCell"];
     cell.selectedBackgroundView = [self cellBackgroundView];
    }
  }
  else if (indexPath.section == 1)
  {
    if (indexPath.row == [self.fetchedResultsController.fetchedObjects count] || [self.fetchedResultsController.fetchedObjects count] == 0)
    {
      cell = [tableView dequeueReusableCellWithIdentifier:@"addStepCell"];
      cell.selectedBackgroundView = [self cellBackgroundView];
    }
    else
    {
      cell = [tableView dequeueReusableCellWithIdentifier:@"stepCell"];
      NSIndexPath *indexPathForStepObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
      cell.selectedBackgroundView = [self cellBackgroundView];

      [self configureCell:cell atIndexPath:indexPathForStepObject];
    }
  }
  
  return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete)
  {
    NSIndexPath *indexPathForStepObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    LQStep *step = [self.fetchedResultsController objectAtIndexPath:indexPathForStepObject];
    
    [self.currentLongTermQuest removeStep:step];
    [[LQSharedManager sharedManager].managedObjectContext deleteObject:step];
    
    [[LQSharedManager sharedManager] saveContext];
  }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
  LQStep *step = [self.fetchedResultsController objectAtIndexPath:indexPath];
  
  [self configureCheckmarkForCell:cell withStep:step];
  [self configureTextForCell:cell withStep:step];
}

- (void)configureCheckmarkForCell:(UITableViewCell *)cell withStep:(LQStep *)theStep
{
  UIButton *button = (UIButton *)[cell viewWithTag:1111];
  
  if (theStep.checkedValue)
  {
    [button setImage:[UIImage imageNamed:@"CheckGreen"] forState:UIControlStateNormal];
  }
  else
  {
    [button setImage:[UIImage imageNamed:@"UncheckGreen"] forState:UIControlStateNormal];
  }
}

- (void)configureTextForCell:(UITableViewCell *)cell withStep:(LQStep *)theStep
{
  UILabel *label = (UILabel *)[cell viewWithTag:1000];
  label.text = theStep.title;
  
  UILabel *dateTextLabel = (UILabel *)[cell viewWithTag:1005];
  dateTextLabel.text = [self formatDate:theStep.dueDate];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
  NSIndexPath *newIndexPathForMO = [NSIndexPath indexPathForRow:newIndexPath.row inSection:1];
  NSIndexPath *updateIndexPathForMO = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
  
  switch (type)
  {
    case NSFetchedResultsChangeInsert:
      [self.tableView insertRowsAtIndexPaths:@[newIndexPathForMO] withRowAnimation:UITableViewRowAnimationFade];
      break;
      
    case NSFetchedResultsChangeDelete:
      [self.tableView deleteRowsAtIndexPaths:@[updateIndexPathForMO] withRowAnimation:UITableViewRowAnimationFade];
      break;
      
    case NSFetchedResultsChangeUpdate:
      [self configureCell:[self.tableView cellForRowAtIndexPath:updateIndexPathForMO] atIndexPath:indexPath];
      break;

    default:
      break;
  }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView endUpdates];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.section == 0 && indexPath.row != 2)
  {
    return nil;
  }
  else
  {
    return indexPath;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self.fetchedResultsController.fetchedObjects count] == 0)
  {
    return UITableViewCellEditingStyleNone;
  }
  else if (indexPath.section == 0)
  {
    return UITableViewCellEditingStyleNone;
  }
  else if (indexPath.row == [self.fetchedResultsController.fetchedObjects count])
  {
    return UITableViewCellEditingStyleNone;
  }
  else
  {
    return UITableViewCellEditingStyleDelete;
  }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addStepSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQStepDetailViewController *vc = (LQStepDetailViewController *)nc.topViewController;
    vc.quest = self.currentLongTermQuest;
  }
  else if ([segue.identifier isEqualToString:@"editStepSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQStepDetailViewController *vc = (LQStepDetailViewController *)nc.topViewController;
    vc.quest = self.currentLongTermQuest;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    NSIndexPath *indexPathForStepObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    
    vc.stepToEdit = [self.fetchedResultsController objectAtIndexPath:indexPathForStepObject];
  }
  else if ([segue.identifier isEqualToString:@"editQuestSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQLongTermQuestInfoTableViewController *vc = (LQLongTermQuestInfoTableViewController *)nc.topViewController;
    vc.currentLongTermQuest = self.currentLongTermQuest;
  }
}

- (NSString *)formatDate:(NSDate *)theDate
{
  static NSDateFormatter *formatter = nil;
  if (!formatter) {
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
  }
  return [formatter stringFromDate:theDate];
}

- (UIView *)cellBackgroundView
{
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  return selectionView;
}

@end
