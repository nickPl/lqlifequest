//
//  SharedManager.m
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQSharedManager.h"
#import "LQLongTermLifeQuest.h"
#import "LQPositiveShortTermLifeQuest.h"
#import "LQNegativeShortTermLifeQuest.h"

@implementation LQSharedManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - Lifecycle

+ (LQSharedManager *)sharedManager
{
  static LQSharedManager* manager = nil;
  
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    manager = [[LQSharedManager alloc] init];
  });
  
  return manager;
}

- (void)saveContext
{
  NSError *error = nil;
  NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
  if (managedObjectContext != nil)
  {
    if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
    {
      NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      abort();
    }
  }
}

#pragma mark - Quests 

- (void)initializeQuestLists
{
  for (int i = 0; i < 40; i++)
  {
    LQLongTermLifeQuest *longTermQuest = [LQLongTermLifeQuest insertInManagedObjectContext:self.managedObjectContext];
    [longTermQuest createWithTitle:[NSString stringWithFormat:@"L-T Quest №%d",i]];
    
    LQPositiveShortTermLifeQuest *positiveQuest = [LQPositiveShortTermLifeQuest insertInManagedObjectContext:self.managedObjectContext];
    [positiveQuest createWithTitle:[NSString stringWithFormat:@"+S-T Quest №%d",i]];
    
    LQNegativeShortTermLifeQuest *negativeQuest = [LQNegativeShortTermLifeQuest insertInManagedObjectContext:self.managedObjectContext];
    [negativeQuest createWithTitle:[NSString stringWithFormat:@"-S-T Quest №%d",i]];
    
    [self saveContext];
  }
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"InitializeQuestLists"];
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
  if (_managedObjectContext != nil)
  {
    return _managedObjectContext;
  }
  
  NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
  if (coordinator != nil)
  {
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
  }
  return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
  if (_managedObjectModel != nil)
  {
    return _managedObjectModel;
  }
  NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LifeQuest" withExtension:@"momd"];
  _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
  return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
  if (_persistentStoreCoordinator != nil)
  {
    return _persistentStoreCoordinator;
  }
  
  NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LifeQuest.sqlite"];
  
  NSError *error = nil;
  _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
  if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
  {
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    abort();
  }
  return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
  return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
