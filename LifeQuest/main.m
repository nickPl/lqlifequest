//
//  main.m
//  LifeQuest
//
//  Created by Plumb on 15.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LQAppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([LQAppDelegate class]));
  }
}
