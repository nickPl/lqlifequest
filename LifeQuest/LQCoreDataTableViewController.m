//
//  LQCoreDataTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 16.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQCoreDataTableViewController.h"
#import "LQSharedManager.h"
#import "LQLifeQuest.h"

@interface LQCoreDataTableViewController ()

@property (assign, nonatomic) NSInteger numberOfQuestsToSelect;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) UIBarButtonItem *nextButton;
@property (weak, nonatomic) UILabel *headerLabel;

@end

@implementation LQCoreDataTableViewController

#pragma mark - Custome Accessors

- (NSManagedObjectContext*)managedObjectContext
{
  if (!_managedObjectContext)
  {
    _managedObjectContext = [[LQSharedManager sharedManager] managedObjectContext];
  }
  return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController
{
  return nil;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.tableView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
  self.tableView.separatorColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]initWithTitle:@"Next"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(nextAction)];
  
  
  [nextButton setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"AvenirNext-DemiBold" size:18]}
                            forState:UIControlStateNormal];
  nextButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
    
  self.nextButton = nextButton;
  self.nextButton.enabled = NO;
  
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back Arrow"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(backAction)];
  backButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
  
  self.navigationItem.leftBarButtonItem = backButton;
  self.navigationItem.rightBarButtonItem = self.nextButton;
  
  self.selectedQuestsArray = [[NSMutableArray alloc]init];
  self.numberOfQuestsToSelect = [self numberOfQuestsToSelect];
  [self performFetch];
}

- (void)dealloc
{
  _fetchedResultsController.delegate = nil;
}

#pragma mark - Action

- (void)nextAction { }
- (void)backAction { }

#pragma mark - FetchedResultsController

- (void)performFetch
{
  NSError *error;
  if (![self.fetchedResultsController performFetch:&error])
  {
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    return;
  }
}

- (NSString *)titleForHeder
{
  return [NSString stringWithFormat:@"SELECTED QUESTS: %ld / %ld    ",[self.selectedQuestsArray count], self.numberOfQuestsToSelect];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  return section == 0 ? nil: [self titleForHeder];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return section == 0 ? 1 : [self.fetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = nil;
  
  if (indexPath.section == 0)
  {
    cell = [tableView dequeueReusableCellWithIdentifier:@"AddNewQuestIdentifier"];
    cell.textLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:20];
    cell.textLabel.text = @"Write my own";
  }
  else
  {
    cell = [tableView dequeueReusableCellWithIdentifier:@"QuestCellIdentifier"];
    NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self configureCell:cell atIndexPath:indexPathForManagedObject];
  }
  
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  cell.selectedBackgroundView = selectionView;
  cell.backgroundColor = [UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:1];
  cell.textLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
  
  return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
  LQLifeQuest *quest = [self.fetchedResultsController objectAtIndexPath:indexPath];
  
  cell.textLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:18];
  cell.textLabel.text = quest.title;
  
  if ([self.selectedQuestsArray containsObject:quest])
  {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  }
  else
  {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }
}

- (void)configureCheckmarkForCell:(UITableViewCell *)theCell withQuest:(LQLifeQuest *)theQuest
{
  if (theCell.accessoryType == UITableViewCellAccessoryNone)
  {
    theCell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.selectedQuestsArray addObject:theQuest];
  }
  else
  {
    theCell.accessoryType = UITableViewCellAccessoryNone;
    [self.selectedQuestsArray removeObject:theQuest];
  }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  if (indexPath.section == 0)
  {
    return;
  }
  
  if (self.numberOfQuestsToSelect == 1)
  {
    if ([self.selectedIndexPath isEqual:indexPath])
    {
      UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
      NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
      LQLifeQuest *quest = [self.fetchedResultsController objectAtIndexPath:indexPathForManagedObject];
      [self configureCheckmarkForCell:cell withQuest:quest];
      
      [self checkSelectedQuests];
      self.selectedIndexPath = nil;
    }
    else if (self.selectedIndexPath != nil)
    {
      UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:self.selectedIndexPath];
      NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:self.selectedIndexPath.row inSection:0];
      LQLifeQuest *oldQuest = [self.fetchedResultsController objectAtIndexPath:indexPathForManagedObject];
      [self configureCheckmarkForCell:oldCell withQuest:oldQuest];
      
      UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
      NSIndexPath *indexPathForNewManagedObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
      LQLifeQuest *quest = [self.fetchedResultsController objectAtIndexPath:indexPathForNewManagedObject];
      [self configureCheckmarkForCell:newCell withQuest:quest];
      
      [self checkSelectedQuests];
      self.selectedIndexPath = indexPath;
    }
    else
    {
      UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
      NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
      LQLifeQuest *quest = [self.fetchedResultsController objectAtIndexPath:indexPathForManagedObject];
      [self configureCheckmarkForCell:cell withQuest:quest];
      
      [self checkSelectedQuests];
      self.selectedIndexPath = indexPath;
    }
  }
  else
  {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    LQLifeQuest *quest = [self.fetchedResultsController objectAtIndexPath:indexPathForManagedObject];
    
    if ([self canSelectQuest])
    {
      [self configureCheckmarkForCell:cell withQuest:quest];
      [self checkSelectedQuests];
    }
    else if (![self canSelectQuest])
    {
      if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
      {
        [self configureCheckmarkForCell:cell withQuest:quest];
        [self checkSelectedQuests];
      }
      else
      {
        return;
      }
    }
  }
  [tableView headerViewForSection:1].textLabel.text = [self titleForHeder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 20.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
  return section == 0 ? 20.f : 0;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
  UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
  header.contentView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
  header.textLabel.textColor = [UIColor colorWithRed:0 green:0.59 blue:0.35 alpha:1];
  header.textLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:16];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
  UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
  footer.contentView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
  NSIndexPath *indexPathForManagedObject = [NSIndexPath indexPathForRow:newIndexPath.row inSection:1];
  
  switch (type)
  {
    case NSFetchedResultsChangeInsert:
      [self.tableView insertRowsAtIndexPaths:@[indexPathForManagedObject] withRowAnimation:UITableViewRowAnimationFade];
      break;
    default:
      break;
  }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView endUpdates];
}

#pragma mark - Private

- (void)checkSelectedQuests
{
  if (![self canSelectQuest])
  {
    self.nextButton.enabled = YES;
  }
  else
  {
    self.nextButton.enabled = NO;
  }
}

- (BOOL)canSelectQuest
{
  return self.numberOfQuestsToSelect == [self.selectedQuestsArray count] ? NO : YES;
}

@end
