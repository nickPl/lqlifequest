//
//  LQNegativeShortTermQuestsCDTVC.m
//  LifeQuest
//
//  Created by Plumb on 16.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQNegativeShortTermQuestsCDTVC.h"
#import "LQNegativeShortTermLifeQuest.h"
#import "LQQuestDetailTableViewController.h"
#import "LQRemindOptionsTableViewController.h"

@interface LQNegativeShortTermQuestsCDTVC ()

@end

@implementation LQNegativeShortTermQuestsCDTVC

@synthesize fetchedResultsController = _fetchedResultsController;

#pragma mark - Custome Accessors

- (NSFetchedResultsController *)fetchedResultsController
{
  if (_fetchedResultsController == nil)
  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([LQNegativeShortTermLifeQuest class])
                                                         inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    
    NSSortDescriptor *titleDescriptor = [[NSSortDescriptor alloc]initWithKey:@"title" ascending:YES];
    [fetchRequest setSortDescriptors:@[titleDescriptor]];
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:self.managedObjectContext
                                                                     sectionNameKeyPath:nil
                                                                              cacheName:nil];
    _fetchedResultsController.delegate = self;
  }
  return  _fetchedResultsController;
}

#pragma mark - Public

- (NSInteger)numberOfQuestsToSelect
{
  return self.numberOfWeeks / 12;
}

#pragma mark - Action

- (void)nextAction
{
  [self performSegueWithIdentifier:@"addRemindingOptionsSegue" sender:nil];
}

- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addNewQuestSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQQuestDetailTableViewController *tvc = (LQQuestDetailTableViewController *)nc.topViewController;
    tvc.questClass = NSStringFromClass([LQNegativeShortTermLifeQuest class]);
  }
  else if ([segue.identifier isEqualToString:@"addRemindingOptionsSegue"])
  {
    LQRemindOptionsTableViewController *tvc = (LQRemindOptionsTableViewController *)segue.destinationViewController;
    tvc.numberOfWeeks = self.numberOfWeeks;
    tvc.allQuestsArray = [self.allQuestsArray arrayByAddingObjectsFromArray:self.selectedQuestsArray];
  }
}

@end
