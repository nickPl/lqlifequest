//
//  LQDailyRemindsTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQDailyRemindsInfoTableViewController.h"
#import "LQWeekDayRemind.h"
#import "LQSharedManager.h"
#import "LQDailyRemindsTableViewController.h"
#import "LQRemindOptionsTableViewController.h"
#import "LQLifePlan.h"

@interface LQDailyRemindsInfoTableViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *addRemindersSwitch;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *daysCell;

@end

@implementation LQDailyRemindsInfoTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  self.daysCell.selectedBackgroundView = selectionView;
  self.addRemindersSwitch.onTintColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.91 alpha:1];
  
  if ([[NSUserDefaults standardUserDefaults]boolForKey:@"FirstTime"])
  {
    [self.navigationItem setRightBarButtonItems:nil animated:YES];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back Arrow"]
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(backAction)];
    backButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
    self.navigationItem.leftBarButtonItem = backButton;
  }
  
  if (self.weekDaysArray)
  {
    //[self createRemindersForAllWeekDays];
    [self updateUI];
  }
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  UIViewController *vc = self.navigationController.topViewController;
  if ([LQRemindOptionsTableViewController class] == [vc class])
  {
    [self performSegueWithIdentifier:@"dailyRemindsInfoUnwindSegue" sender:nil];
  }
}

#pragma mark - UI

- (void)updateUI
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
  [dateFormatter setDateFormat:@"EEE"];
  
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear |
                                                    NSCalendarUnitHour | NSCalendarUnitMinute
                                           fromDate:[NSDate date]];
  
  NSString *resultStr = @"";
  NSInteger numberOfReminds = 0;
  
  for (LQWeekDayRemind *remind in self.weekDaysArray)
  {
    if (remind.shouldRemindValue)
    {
      self.addRemindersSwitch.on = YES;
      numberOfReminds++;
      
      if (numberOfReminds == 7)
      {
        resultStr = @"Every day  ";
      }
      else
      {
        [dateComp setWeekday:remind.weekDayValue];
        NSDate *date = [calendar dateFromComponents:dateComp];
        resultStr = [NSString stringWithFormat:@"%@%@, ",resultStr, [dateFormatter stringFromDate:date]];
      }
    }
  }
  
  if (numberOfReminds == 0)
  {
    self.addRemindersSwitch.on = NO;
  }
  else
  {
    self.daysLabel.text = [resultStr substringToIndex:[resultStr length] - 2];
  }
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneAction:(id)sender
{
  [self.lifePlan scheduleDailyNotifications];
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addRemindersSwitchValueChanged:(UISwitch *)sender
{
  if (sender.on)
  {
    [self showSelectedDaysCell];
    
    if (!self.weekDaysArray)
    {
      [self createRemindersForAllWeekDays];
    }
    else
    {
      [self shouldAddRemindsForAllWeekDays:YES];
    }
  }
  else
  {
    [self hideSelectedDaysCell];
    [self shouldAddRemindsForAllWeekDays:NO];
  }
}

- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Remind Methods

- (void)shouldAddRemindsForAllWeekDays:(BOOL)add
{
  for (LQWeekDayRemind *remind in self.weekDaysArray)
  {
    [remind setShouldRemindValue:add];
    [[LQSharedManager sharedManager] saveContext];
  }
}

- (void)createRemindersForAllWeekDays
{
  self.weekDaysArray = [[NSMutableArray alloc]initWithCapacity:7];
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute
                                           fromDate:[NSDate date]];
  NSDate *fireTime = [calendar dateFromComponents:dateComp];
  
  for (int i = 1; i <= 7; i++)
  {
    LQWeekDayRemind *dayRemind = [LQWeekDayRemind insertInManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];
    [dayRemind createWithWeekDay:i andTime:fireTime];
    [self.weekDaysArray addObject:dayRemind];
    
    [[LQSharedManager sharedManager] saveContext];
  }
}

#pragma mark - Selected Days Cell

- (void)showSelectedDaysCell
{
  NSIndexPath *indexPathSelectedDaysCell = [NSIndexPath indexPathForRow:1 inSection:0];
  
  [self.tableView beginUpdates];
  [self.tableView insertRowsAtIndexPaths:@[indexPathSelectedDaysCell] withRowAnimation:UITableViewRowAnimationTop];
  [self.tableView endUpdates];
}

- (void)hideSelectedDaysCell
{
  NSIndexPath *indexPathSelectedDaysCell = [NSIndexPath indexPathForRow:1 inSection:0];
  
  [self.tableView beginUpdates];
  [self.tableView deleteRowsAtIndexPaths:@[indexPathSelectedDaysCell] withRowAnimation:UITableViewRowAnimationTop];
  [self.tableView endUpdates];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.addRemindersSwitch.on ? 2 : 1;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.row == 1)
  {
    return indexPath;
  }
  else
  {
    return nil;
  }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"dailyRemindsSegue"])
  {
    LQDailyRemindsTableViewController *tvc = (LQDailyRemindsTableViewController *)segue.destinationViewController;
    tvc.weekDaysArray = self.weekDaysArray;
  }
}

- (IBAction)dailyRemindDidSetReminds:(UIStoryboardSegue *)segue
{
  LQDailyRemindsTableViewController *vc = segue.sourceViewController;
  self.weekDaysArray = vc.weekDaysArray;
  
  [self updateUI];
}

@end
