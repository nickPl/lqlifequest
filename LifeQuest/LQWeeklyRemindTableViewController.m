//
//  LQWeeklyRemindTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQWeeklyRemindTableViewController.h"
#import "LQLifePeriod.h"
#import "LQSharedManager.h"

@interface LQWeeklyRemindTableViewController ()

@property (assign, nonatomic) NSInteger selectedDay;
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *weekDaysButtons;

@end

@implementation LQWeeklyRemindTableViewController

#pragma mark - Action

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back Arrow"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(backAction)];
  backButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
  self.navigationItem.leftBarButtonItem = backButton;
  self.navigationItem.title = @"Weekly Remind";
  
  if (self.periodToEdit)
  {
    [self customizeUI];
  }
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  if (self.selectedDay)
  {
    [self performSegueWithIdentifier:@"weeklyRemindUnwindSegue" sender:nil];
  }
}

- (void)customizeUI
{
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components: NSCalendarUnitYear | NSCalendarUnitWeekOfYear |
                                                     NSCalendarUnitHour | NSCalendarUnitMinute |
                                                     NSCalendarUnitWeekday
                                           fromDate:self.periodToEdit.startDate];
  
  UIButton *button = [self.weekDaysButtons objectAtIndex:dateComp.weekday - 1];

  button.backgroundColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.91 alpha:1];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  self.selectedDay = dateComp.weekday;
  self.timePicker.date = [calendar dateFromComponents:dateComp];
}

#pragma mark - Action

- (IBAction)selectedDayAction:(UIButton *)sender
{  
  if (self.selectedDay)
  {
    UIButton *oldButton = [self.weekDaysButtons objectAtIndex:self.selectedDay - 1];
    oldButton.backgroundColor = [UIColor clearColor];
    oldButton.titleLabel.textColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.91 alpha:1];
    sender.backgroundColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.91 alpha:1];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.selectedDay = [self.weekDaysButtons indexOfObject:sender] + 1;
  }
  else
  {
    sender.backgroundColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.91 alpha:1];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.selectedDay = [self.weekDaysButtons indexOfObject:sender] + 1;
  }
}

- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - LQLifePeriod Methods

- (void)createLifePeriod
{
  LQLifePeriod *lifePeriod = [LQLifePeriod insertInManagedObjectContext:[[LQSharedManager sharedManager]managedObjectContext]];
  [lifePeriod createWithStartDate:[self createStartDate] andNumberOfWeeks:self.numberOfWeeks];
  self.periodToEdit = lifePeriod;
  
  [[LQSharedManager sharedManager]saveContext];
}

- (void)updateLifePeriod
{
  [self.periodToEdit createWithStartDate:[self createStartDate] andNumberOfWeeks:self.numberOfWeeks];
  
  [[LQSharedManager sharedManager]saveContext];
}

- (NSDate *)createStartDate
{
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear |
                                                    NSCalendarUnitHour | NSCalendarUnitMinute
                                           fromDate:self.timePicker.date];
  [dateComp setWeekday:self.selectedDay];
  
  NSDate *date = [calendar dateFromComponents:dateComp];
  
  if ([date compare:[NSDate date]] == NSOrderedAscending)
  {
    date = [calendar dateByAddingUnit:NSCalendarUnitWeekOfYear value:1 toDate:date options:0];
  }
  return date;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  return nil;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"weeklyRemindUnwindSegue"])
  {
    if (self.periodToEdit)
    {
      [self updateLifePeriod];
    }
    else
    {
      [self createLifePeriod];
    }
  }
}

@end
