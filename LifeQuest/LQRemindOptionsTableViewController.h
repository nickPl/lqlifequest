//
//  LQRemindOptionsTableViewController.h
//  LifeQuest
//
//  Created by Plumb on 18.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLifePlan;

@interface LQRemindOptionsTableViewController : UITableViewController

@property (assign, nonatomic) NSInteger numberOfWeeks;
@property (strong, nonatomic) NSArray *allQuestsArray;

@end
