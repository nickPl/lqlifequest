//
//  LQWeeklyQuestNoteTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 23.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQWeeklyQuestNoteTableViewController.h"
#import "LQLifeQuest.h"
#import "LQSharedManager.h"

@interface LQWeeklyQuestNoteTableViewController ()

@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@end

@implementation LQWeeklyQuestNoteTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  if (self.quest.note)
  {
    self.noteTextView.text = self.quest.note;
    self.saveButton.enabled = YES;
  }
  else
  {
    self.noteTextView.text = @"";
    self.saveButton.enabled = NO;
  }
  
  [self.noteTextView becomeFirstResponder];
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender
{
  self.quest.note = self.noteTextView.text;
  
  [[LQSharedManager sharedManager]saveContext];
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
  NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
  
  self.saveButton.enabled = ([newText length] > 0);
  
  return YES;
}

@end
