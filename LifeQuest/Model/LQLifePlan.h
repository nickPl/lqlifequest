#import "_LQLifePlan.h"
#import <UIKit/UIKit.h>

@class LQLifePeriod;
@class LQLifeQuest;

@interface LQLifePlan : _LQLifePlan {}

- (void)createWithPlannedPeriod:(LQLifePeriod *)thePeriod
             selectedLifeQuests:(NSArray *)theQuests
              andDailyReminders:(NSArray *)theReminders;
- (void)addNewQuest:(LQLifeQuest *)newQuest;
- (void)generateWeeklyQuest;

- (void)scheduleWeeklyNotification;
- (void)scheduleDailyNotifications;
- (void)handlingWeeklyLocalNotification:(UILocalNotification *)weeklyNotification;

@end
