#import "_LQLifePeriod.h"

@interface LQLifePeriod : _LQLifePeriod {}

- (void)createWithStartDate:(NSDate *)theStartDate
           andNumberOfWeeks:(NSInteger)theNumberOfWeeks;
- (void)incrementPeriod;

@end
