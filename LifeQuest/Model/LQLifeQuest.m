#import "LQLifeQuest.h"

@interface LQLifeQuest ()

// Private interface goes here.

@end

@implementation LQLifeQuest

- (void)createWithTitle:(NSString *)theTitle
{
  [self setTitle:theTitle];
}

@end
