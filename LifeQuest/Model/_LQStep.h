// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQStep.h instead.

#import <CoreData/CoreData.h>

extern const struct LQStepAttributes {
	__unsafe_unretained NSString *checked;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *dueDate;
	__unsafe_unretained NSString *title;
} LQStepAttributes;

extern const struct LQStepRelationships {
	__unsafe_unretained NSString *quest;
} LQStepRelationships;

@class LQLongTermLifeQuest;

@interface LQStepID : NSManagedObjectID {}
@end

@interface _LQStep : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQStepID* objectID;

@property (nonatomic, strong) NSNumber* checked;

@property (atomic) BOOL checkedValue;
- (BOOL)checkedValue;
- (void)setCheckedValue:(BOOL)value_;

//- (BOOL)validateChecked:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dueDate;

//- (BOOL)validateDueDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLongTermLifeQuest *quest;

//- (BOOL)validateQuest:(id*)value_ error:(NSError**)error_;

@end

@interface _LQStep (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveChecked;
- (void)setPrimitiveChecked:(NSNumber*)value;

- (BOOL)primitiveCheckedValue;
- (void)setPrimitiveCheckedValue:(BOOL)value_;

- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;

- (NSDate*)primitiveDueDate;
- (void)setPrimitiveDueDate:(NSDate*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (LQLongTermLifeQuest*)primitiveQuest;
- (void)setPrimitiveQuest:(LQLongTermLifeQuest*)value;

@end
