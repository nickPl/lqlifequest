#import "LQLongTermLifeQuest.h"
#import "LQStep.h"

@interface LQLongTermLifeQuest ()

// Private interface goes here.

@end

@implementation LQLongTermLifeQuest

- (void)addStep:(LQStep *)theStep
{
  [self addStepsObject:theStep];
  [self countProgress];
}
- (void)removeStep:(LQStep *)theStep
{
  [self removeStepsObject:theStep];
  [self countProgress];
}

- (void)countProgress
{
  NSInteger checkedSteps = [self countCheckedSteps];
  NSInteger allSteps = [self.steps count];
  NSInteger progress = lroundf(checkedSteps / (float)allSteps * 100);
  
  progress = progress < 0 ? 0 : progress;
  
  self.progressValue = progress;
}

- (NSInteger)countCheckedSteps
{
  NSInteger count = 0;
  
  for (LQStep *step in self.steps)
  {
    if (step.checkedValue)
    {
      count ++;
    }
  }
  return count;
}

@end
