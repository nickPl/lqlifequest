// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQWeekDayRemind.m instead.

#import "_LQWeekDayRemind.h"

const struct LQWeekDayRemindAttributes LQWeekDayRemindAttributes = {
	.fireTime = @"fireTime",
	.shouldRemind = @"shouldRemind",
	.weekDay = @"weekDay",
};

const struct LQWeekDayRemindRelationships LQWeekDayRemindRelationships = {
	.userPlan = @"userPlan",
};

@implementation LQWeekDayRemindID
@end

@implementation _LQWeekDayRemind

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQWeekDayRemind" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQWeekDayRemind";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQWeekDayRemind" inManagedObjectContext:moc_];
}

- (LQWeekDayRemindID*)objectID {
	return (LQWeekDayRemindID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"shouldRemindValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shouldRemind"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"weekDayValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"weekDay"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic fireTime;

@dynamic shouldRemind;

- (BOOL)shouldRemindValue {
	NSNumber *result = [self shouldRemind];
	return [result boolValue];
}

- (void)setShouldRemindValue:(BOOL)value_ {
	[self setShouldRemind:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShouldRemindValue {
	NSNumber *result = [self primitiveShouldRemind];
	return [result boolValue];
}

- (void)setPrimitiveShouldRemindValue:(BOOL)value_ {
	[self setPrimitiveShouldRemind:[NSNumber numberWithBool:value_]];
}

@dynamic weekDay;

- (int64_t)weekDayValue {
	NSNumber *result = [self weekDay];
	return [result longLongValue];
}

- (void)setWeekDayValue:(int64_t)value_ {
	[self setWeekDay:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveWeekDayValue {
	NSNumber *result = [self primitiveWeekDay];
	return [result longLongValue];
}

- (void)setPrimitiveWeekDayValue:(int64_t)value_ {
	[self setPrimitiveWeekDay:[NSNumber numberWithLongLong:value_]];
}

@dynamic userPlan;

@end

