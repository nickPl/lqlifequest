// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQPositiveShortTermLifeQuest.h instead.

#import <CoreData/CoreData.h>
#import "LQLifeQuest.h"

@interface LQPositiveShortTermLifeQuestID : LQLifeQuestID {}
@end

@interface _LQPositiveShortTermLifeQuest : LQLifeQuest {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQPositiveShortTermLifeQuestID* objectID;

@end

@interface _LQPositiveShortTermLifeQuest (CoreDataGeneratedPrimitiveAccessors)

@end
