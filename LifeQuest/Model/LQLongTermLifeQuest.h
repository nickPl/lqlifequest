#import "_LQLongTermLifeQuest.h"

@interface LQLongTermLifeQuest : _LQLongTermLifeQuest {}

- (void)addStep:(LQStep *)theStep;
- (void)removeStep:(LQStep *)theStep;
- (void)countProgress;

@end
