// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifeQuest.h instead.

#import <CoreData/CoreData.h>

extern const struct LQLifeQuestAttributes {
	__unsafe_unretained NSString *note;
	__unsafe_unretained NSString *questDescription;
	__unsafe_unretained NSString *title;
} LQLifeQuestAttributes;

extern const struct LQLifeQuestRelationships {
	__unsafe_unretained NSString *plan;
	__unsafe_unretained NSString *userPlan;
} LQLifeQuestRelationships;

@class LQLifePlan;
@class LQLifePlan;

@interface LQLifeQuestID : NSManagedObjectID {}
@end

@interface _LQLifeQuest : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQLifeQuestID* objectID;

@property (nonatomic, strong) NSString* note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* questDescription;

//- (BOOL)validateQuestDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLifePlan *plan;

//- (BOOL)validatePlan:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLifePlan *userPlan;

//- (BOOL)validateUserPlan:(id*)value_ error:(NSError**)error_;

@end

@interface _LQLifeQuest (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveNote;
- (void)setPrimitiveNote:(NSString*)value;

- (NSString*)primitiveQuestDescription;
- (void)setPrimitiveQuestDescription:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (LQLifePlan*)primitivePlan;
- (void)setPrimitivePlan:(LQLifePlan*)value;

- (LQLifePlan*)primitiveUserPlan;
- (void)setPrimitiveUserPlan:(LQLifePlan*)value;

@end
