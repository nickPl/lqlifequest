// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLongTermLifeQuest.m instead.

#import "_LQLongTermLifeQuest.h"

const struct LQLongTermLifeQuestAttributes LQLongTermLifeQuestAttributes = {
	.progress = @"progress",
};

const struct LQLongTermLifeQuestRelationships LQLongTermLifeQuestRelationships = {
	.lifePlan = @"lifePlan",
	.steps = @"steps",
};

@implementation LQLongTermLifeQuestID
@end

@implementation _LQLongTermLifeQuest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQLongTermLifeQuest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQLongTermLifeQuest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQLongTermLifeQuest" inManagedObjectContext:moc_];
}

- (LQLongTermLifeQuestID*)objectID {
	return (LQLongTermLifeQuestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"progressValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"progress"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic progress;

- (int64_t)progressValue {
	NSNumber *result = [self progress];
	return [result longLongValue];
}

- (void)setProgressValue:(int64_t)value_ {
	[self setProgress:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveProgressValue {
	NSNumber *result = [self primitiveProgress];
	return [result longLongValue];
}

- (void)setPrimitiveProgressValue:(int64_t)value_ {
	[self setPrimitiveProgress:[NSNumber numberWithLongLong:value_]];
}

@dynamic lifePlan;

@dynamic steps;

- (NSMutableSet*)stepsSet {
	[self willAccessValueForKey:@"steps"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"steps"];

	[self didAccessValueForKey:@"steps"];
	return result;
}

@end

