// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQWeekDayRemind.h instead.

#import <CoreData/CoreData.h>

extern const struct LQWeekDayRemindAttributes {
	__unsafe_unretained NSString *fireTime;
	__unsafe_unretained NSString *shouldRemind;
	__unsafe_unretained NSString *weekDay;
} LQWeekDayRemindAttributes;

extern const struct LQWeekDayRemindRelationships {
	__unsafe_unretained NSString *userPlan;
} LQWeekDayRemindRelationships;

@class LQLifePlan;

@interface LQWeekDayRemindID : NSManagedObjectID {}
@end

@interface _LQWeekDayRemind : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQWeekDayRemindID* objectID;

@property (nonatomic, strong) NSDate* fireTime;

//- (BOOL)validateFireTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* shouldRemind;

@property (atomic) BOOL shouldRemindValue;
- (BOOL)shouldRemindValue;
- (void)setShouldRemindValue:(BOOL)value_;

//- (BOOL)validateShouldRemind:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* weekDay;

@property (atomic) int64_t weekDayValue;
- (int64_t)weekDayValue;
- (void)setWeekDayValue:(int64_t)value_;

//- (BOOL)validateWeekDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLifePlan *userPlan;

//- (BOOL)validateUserPlan:(id*)value_ error:(NSError**)error_;

@end

@interface _LQWeekDayRemind (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveFireTime;
- (void)setPrimitiveFireTime:(NSDate*)value;

- (NSNumber*)primitiveShouldRemind;
- (void)setPrimitiveShouldRemind:(NSNumber*)value;

- (BOOL)primitiveShouldRemindValue;
- (void)setPrimitiveShouldRemindValue:(BOOL)value_;

- (NSNumber*)primitiveWeekDay;
- (void)setPrimitiveWeekDay:(NSNumber*)value;

- (int64_t)primitiveWeekDayValue;
- (void)setPrimitiveWeekDayValue:(int64_t)value_;

- (LQLifePlan*)primitiveUserPlan;
- (void)setPrimitiveUserPlan:(LQLifePlan*)value;

@end
