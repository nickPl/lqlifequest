// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifePlan.m instead.

#import "_LQLifePlan.h"

const struct LQLifePlanRelationships LQLifePlanRelationships = {
	.currentLongTermQuests = @"currentLongTermQuests",
	.currentWeeklyQuest = @"currentWeeklyQuest",
	.dailyReminds = @"dailyReminds",
	.lifeQuests = @"lifeQuests",
	.plannedPeriod = @"plannedPeriod",
};

@implementation LQLifePlanID
@end

@implementation _LQLifePlan

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQLifePlan" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQLifePlan";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQLifePlan" inManagedObjectContext:moc_];
}

- (LQLifePlanID*)objectID {
	return (LQLifePlanID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic currentLongTermQuests;

- (NSMutableSet*)currentLongTermQuestsSet {
	[self willAccessValueForKey:@"currentLongTermQuests"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"currentLongTermQuests"];

	[self didAccessValueForKey:@"currentLongTermQuests"];
	return result;
}

@dynamic currentWeeklyQuest;

@dynamic dailyReminds;

- (NSMutableSet*)dailyRemindsSet {
	[self willAccessValueForKey:@"dailyReminds"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"dailyReminds"];

	[self didAccessValueForKey:@"dailyReminds"];
	return result;
}

@dynamic lifeQuests;

- (NSMutableSet*)lifeQuestsSet {
	[self willAccessValueForKey:@"lifeQuests"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"lifeQuests"];

	[self didAccessValueForKey:@"lifeQuests"];
	return result;
}

@dynamic plannedPeriod;

@end

