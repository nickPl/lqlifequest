// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQStep.m instead.

#import "_LQStep.h"

const struct LQStepAttributes LQStepAttributes = {
	.checked = @"checked",
	.creationDate = @"creationDate",
	.dueDate = @"dueDate",
	.title = @"title",
};

const struct LQStepRelationships LQStepRelationships = {
	.quest = @"quest",
};

@implementation LQStepID
@end

@implementation _LQStep

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQStep" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQStep";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQStep" inManagedObjectContext:moc_];
}

- (LQStepID*)objectID {
	return (LQStepID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"checkedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"checked"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic checked;

- (BOOL)checkedValue {
	NSNumber *result = [self checked];
	return [result boolValue];
}

- (void)setCheckedValue:(BOOL)value_ {
	[self setChecked:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCheckedValue {
	NSNumber *result = [self primitiveChecked];
	return [result boolValue];
}

- (void)setPrimitiveCheckedValue:(BOOL)value_ {
	[self setPrimitiveChecked:[NSNumber numberWithBool:value_]];
}

@dynamic creationDate;

@dynamic dueDate;

@dynamic title;

@dynamic quest;

@end

