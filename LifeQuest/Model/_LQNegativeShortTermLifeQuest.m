// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQNegativeShortTermLifeQuest.m instead.

#import "_LQNegativeShortTermLifeQuest.h"

@implementation LQNegativeShortTermLifeQuestID
@end

@implementation _LQNegativeShortTermLifeQuest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQNegativeShortTermLifeQuest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQNegativeShortTermLifeQuest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQNegativeShortTermLifeQuest" inManagedObjectContext:moc_];
}

- (LQNegativeShortTermLifeQuestID*)objectID {
	return (LQNegativeShortTermLifeQuestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

