#import "LQLifePlan.h"
#import "LQLifePeriod.h"
#import "LQLifeQuest.h"
#import "LQLongTermLifeQuest.h"
#import "LQSharedManager.h"
#import "LQWeekDayRemind.h"

@interface LQLifePlan ()

// Private interface goes here.

@end

@implementation LQLifePlan

#pragma mark - Lifecycle

- (void)createWithPlannedPeriod:(LQLifePeriod *)thePeriod
             selectedLifeQuests:(NSArray *)theQuests
              andDailyReminders:(NSArray *)theReminders
{
  self.plannedPeriod = thePeriod;
  self.lifeQuests = [NSSet setWithArray:theQuests];
  self.dailyReminds = [NSSet setWithArray:theReminders];
  
  [self generateWeeklyQuest];
  [self scheduleWeeklyNotification];
  [self scheduleDailyNotifications];
}

#pragma mark - Quests Methods

- (void)generateWeeklyQuest
{

//#warning Generate Weekly Quest

  LQLifeQuest *currentQuest = [self.lifeQuests anyObject];
  
  if ([currentQuest isMemberOfClass:[LQLongTermLifeQuest class]])
  {
    [self addCurrentLongTermQuestsObject:(LQLongTermLifeQuest *)currentQuest];
  }
  
  self.currentWeeklyQuest = currentQuest;
  [self removeLifeQuestsObject:currentQuest];
  
  //------------------//
  NSInteger numberOfQuests = [[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"];
  [[NSUserDefaults standardUserDefaults]setInteger:--numberOfQuests forKey:@"NumberOfQuests"];
  NSLog(@"NumberOfQuests: %ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"]);
  //------------------//
  
  [[LQSharedManager sharedManager] saveContext];
/*
#warning LOG #5 (WEEKLY Quest)
  
  //--------------LOG-------------//
  NSLog(@"------------WEEKLY QUEST-----------");
  NSLog(@"This week quest is: %@", self.currentWeeklyQuest.title);
  NSLog(@" ");
  //------------------------------//
 */
}

- (void)addNewQuest:(LQLifeQuest *)newQuest
{
  //------------------//
  NSInteger numberOfQuests = [[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"];
  [[NSUserDefaults standardUserDefaults]setInteger:++numberOfQuests forKey:@"NumberOfQuests"];
  NSLog(@"NumberOfQuests: %ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"NumberOfQuests"]);
  //------------------//
  
  [self.lifeQuestsSet addObject:newQuest];
  [self.plannedPeriod incrementPeriod];
}

#pragma mark - Notifications

- (void)scheduleWeeklyNotification
{
  UILocalNotification *weeklyNotification = [[UILocalNotification alloc]init];
  
  weeklyNotification.fireDate = self.plannedPeriod.startDate;
  weeklyNotification.timeZone = [NSTimeZone defaultTimeZone];
  weeklyNotification.alertBody = @"New week, new quest";
  weeklyNotification.soundName = UILocalNotificationDefaultSoundName;
  weeklyNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
  weeklyNotification.userInfo = @{@"NotificationKey": @"Weekly"};
  weeklyNotification.repeatInterval = NSCalendarUnitWeekOfYear;
  
  [[UIApplication sharedApplication]scheduleLocalNotification:weeklyNotification];
/*
#warning LOG #3 (Scheduled WEEKLY Notification)
  
  //--------------LOG-------------//
  NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
  [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
  
  NSLog(@"----------WEEKLY FIRE DATE---------");
  NSLog(@"Scheduled Weekly FIRE DATE: %@", [newDateFormatter stringFromDate:weeklyNotification.fireDate]);
  NSLog(@" ");
  //------------------------------//
 */
}

- (void)scheduleDailyNotifications
{
  for (LQWeekDayRemind *remind in self.dailyReminds)
  {
    [self scheduleNotificationForWeekDayRemind:remind];
  }
}

- (void)scheduleNotificationForWeekDayRemind:(LQWeekDayRemind *)theWeekDayRemind
{
  UILocalNotification *existingNotification = [self notificationForWeekDayRemind:theWeekDayRemind];
  
  if (existingNotification)
  {
/*
#warning LOG #6 (Removing exiting DAILY notification)
    
    //--------------LOG-------------//
    
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
    
    NSLog(@"---------------REMOVE--------------");
    NSLog(@"Removing exiting notification (Fire Date): %@",[newDateFormatter stringFromDate:existingNotification.fireDate]);
    NSLog(@"Alert Body: %@", existingNotification.alertBody);
    NSLog(@" ");
    //------------------------------//
*/
    [[UIApplication sharedApplication]cancelLocalNotification:existingNotification];
  }
  
  if (theWeekDayRemind.shouldRemindValue)
  {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear
                                             fromDate:[NSDate date]];
    NSDateComponents *timeComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute
                                             fromDate:theWeekDayRemind.fireTime];
    [dateComp setWeekday:theWeekDayRemind.weekDayValue];
    [dateComp setHour:timeComp.hour];
    [dateComp setMinute:timeComp.minute];
    
    NSDate *date = [calendar dateFromComponents:dateComp];
    
    if ([date compare:[NSDate date]] == NSOrderedAscending)
    {
      date = [calendar dateByAddingUnit:NSCalendarUnitWeekOfYear value:1 toDate:date options:0];
    }
    
    UILocalNotification *dailyNotification = [[UILocalNotification alloc]init];
    
    dailyNotification.fireDate = date;
    dailyNotification.timeZone = [NSTimeZone defaultTimeZone];
    dailyNotification.alertBody = [NSString stringWithFormat:@"Today Quest: %@",self.currentWeeklyQuest.title];
    dailyNotification.soundName = UILocalNotificationDefaultSoundName;
    dailyNotification.userInfo = @{@"NotificationKey": @"Daily",
                                   @"WeekDayKey"     : @(theWeekDayRemind.weekDayValue)};
    dailyNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:dailyNotification];
/*
#warning LOG #7 (Scheduled DAILY Notification)
    
    //--------------LOG-------------//
    
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
    
    NSLog(@"++++++++++++++++ADD++++++++++++++++");
    NSLog(@"Scheduled Daily FIRE DATE: %@", [newDateFormatter stringFromDate:dailyNotification.fireDate]);
    NSLog(@"Alert Body: %@", dailyNotification.alertBody);
    NSLog(@" ");
    
    //------------------------------//
*/
  }
}

- (UILocalNotification *)notificationForWeekDayRemind:(LQWeekDayRemind *)theWeekDayRemind
{
  NSArray *allNotifications = [[UIApplication sharedApplication]scheduledLocalNotifications];
  
  for (UILocalNotification *notification in allNotifications)
  {
    NSNumber *number = [notification.userInfo objectForKey:@"WeekDayKey"];
    
    if (number && [number integerValue] == theWeekDayRemind.weekDayValue)
    {
      return notification;
    }
  }
  return nil;
}

- (void)handlingWeeklyLocalNotification:(UILocalNotification *)weeklyNotification
{
  if ([self.plannedPeriod.endDate compare:[NSDate date]] == NSOrderedAscending)
  {
    [[UIApplication sharedApplication] cancelLocalNotification:weeklyNotification];
    
    [self generateWeeklyQuest];
    [self scheduleDailyNotifications];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:@"You have no quests"
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    [alert show];
  }
  else
  {
    [self generateWeeklyQuest];
    [self scheduleDailyNotifications];
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
      UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                     message:@"You recieved new week quest"
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
      [alert show];
    }
  }

/*
#warning LOG #4 (Handling WEEKLY Notification)
  
  //--------------LOG-------------//
  
  NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
  [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
  
  
  NSLog(@"Handling Weekly Notification:\n %@", weeklyNotification);
  
  //--------------LOG-------------//
 */
}

@end
