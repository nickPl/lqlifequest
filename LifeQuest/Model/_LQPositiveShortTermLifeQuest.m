// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQPositiveShortTermLifeQuest.m instead.

#import "_LQPositiveShortTermLifeQuest.h"

@implementation LQPositiveShortTermLifeQuestID
@end

@implementation _LQPositiveShortTermLifeQuest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQPositiveShortTermLifeQuest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQPositiveShortTermLifeQuest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQPositiveShortTermLifeQuest" inManagedObjectContext:moc_];
}

- (LQPositiveShortTermLifeQuestID*)objectID {
	return (LQPositiveShortTermLifeQuestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

