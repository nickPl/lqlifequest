#import "_LQStep.h"
#import <UIKit/UIKit.h>

@interface LQStep : _LQStep {}

- (void)createWithTitle:(NSString *)theTitle creationDate:(NSDate *)theCreationDate andDueDate:(NSDate *)theDate;
- (void)toggleChecked;
- (void)scheduleNotification;

@end
