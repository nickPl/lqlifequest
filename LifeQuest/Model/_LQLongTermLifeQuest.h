// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLongTermLifeQuest.h instead.

#import <CoreData/CoreData.h>
#import "LQLifeQuest.h"

extern const struct LQLongTermLifeQuestAttributes {
	__unsafe_unretained NSString *progress;
} LQLongTermLifeQuestAttributes;

extern const struct LQLongTermLifeQuestRelationships {
	__unsafe_unretained NSString *lifePlan;
	__unsafe_unretained NSString *steps;
} LQLongTermLifeQuestRelationships;

@class LQLifePlan;
@class LQStep;

@interface LQLongTermLifeQuestID : LQLifeQuestID {}
@end

@interface _LQLongTermLifeQuest : LQLifeQuest {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQLongTermLifeQuestID* objectID;

@property (nonatomic, strong) NSNumber* progress;

@property (atomic) int64_t progressValue;
- (int64_t)progressValue;
- (void)setProgressValue:(int64_t)value_;

//- (BOOL)validateProgress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLifePlan *lifePlan;

//- (BOOL)validateLifePlan:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *steps;

- (NSMutableSet*)stepsSet;

@end

@interface _LQLongTermLifeQuest (StepsCoreDataGeneratedAccessors)
- (void)addSteps:(NSSet*)value_;
- (void)removeSteps:(NSSet*)value_;
- (void)addStepsObject:(LQStep*)value_;
- (void)removeStepsObject:(LQStep*)value_;

@end

@interface _LQLongTermLifeQuest (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveProgress;
- (void)setPrimitiveProgress:(NSNumber*)value;

- (int64_t)primitiveProgressValue;
- (void)setPrimitiveProgressValue:(int64_t)value_;

- (LQLifePlan*)primitiveLifePlan;
- (void)setPrimitiveLifePlan:(LQLifePlan*)value;

- (NSMutableSet*)primitiveSteps;
- (void)setPrimitiveSteps:(NSMutableSet*)value;

@end
