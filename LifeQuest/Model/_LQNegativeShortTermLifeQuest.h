// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQNegativeShortTermLifeQuest.h instead.

#import <CoreData/CoreData.h>
#import "LQLifeQuest.h"

@interface LQNegativeShortTermLifeQuestID : LQLifeQuestID {}
@end

@interface _LQNegativeShortTermLifeQuest : LQLifeQuest {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQNegativeShortTermLifeQuestID* objectID;

@end

@interface _LQNegativeShortTermLifeQuest (CoreDataGeneratedPrimitiveAccessors)

@end
