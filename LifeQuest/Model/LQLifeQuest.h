#import "_LQLifeQuest.h"

@interface LQLifeQuest : _LQLifeQuest {}
// Custom logic goes here.

- (void)createWithTitle:(NSString *)theTitle;

@end
