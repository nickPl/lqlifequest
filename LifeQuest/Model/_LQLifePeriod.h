// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifePeriod.h instead.

#import <CoreData/CoreData.h>

extern const struct LQLifePeriodAttributes {
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *startDate;
} LQLifePeriodAttributes;

extern const struct LQLifePeriodRelationships {
	__unsafe_unretained NSString *userPlan;
} LQLifePeriodRelationships;

@class LQLifePlan;

@interface LQLifePeriodID : NSManagedObjectID {}
@end

@interface _LQLifePeriod : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQLifePeriodID* objectID;

@property (nonatomic, strong) NSDate* endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) LQLifePlan *userPlan;

//- (BOOL)validateUserPlan:(id*)value_ error:(NSError**)error_;

@end

@interface _LQLifePeriod (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

- (LQLifePlan*)primitiveUserPlan;
- (void)setPrimitiveUserPlan:(LQLifePlan*)value;

@end
