// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifeQuest.m instead.

#import "_LQLifeQuest.h"

const struct LQLifeQuestAttributes LQLifeQuestAttributes = {
	.note = @"note",
	.questDescription = @"questDescription",
	.title = @"title",
};

const struct LQLifeQuestRelationships LQLifeQuestRelationships = {
	.plan = @"plan",
	.userPlan = @"userPlan",
};

@implementation LQLifeQuestID
@end

@implementation _LQLifeQuest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQLifeQuest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQLifeQuest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQLifeQuest" inManagedObjectContext:moc_];
}

- (LQLifeQuestID*)objectID {
	return (LQLifeQuestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic note;

@dynamic questDescription;

@dynamic title;

@dynamic plan;

@dynamic userPlan;

@end

