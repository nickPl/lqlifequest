#import "LQWeekDayRemind.h"

@interface LQWeekDayRemind ()

// Private interface goes here.

@end

@implementation LQWeekDayRemind

- (void)createWithWeekDay:(NSInteger)theWeekDay andTime:(NSDate *)theTime
{
  self.weekDayValue = theWeekDay;
  self.fireTime = theTime;
  [self setShouldRemindValue:YES];
}

@end
