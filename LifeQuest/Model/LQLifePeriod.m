#import "LQLifePeriod.h"
#import "LQSharedManager.h"

@interface LQLifePeriod ()

// Private interface goes here.

@end

@implementation LQLifePeriod

- (void)createWithStartDate:(NSDate *)theStartDate andNumberOfWeeks:(NSInteger)theNumberOfWeeks
{
  self.startDate = theStartDate;
  
  NSCalendar *calendar = [NSCalendar currentCalendar];
  
  self.endDate = [calendar dateByAddingUnit:NSCalendarUnitWeekOfYear
                                      value:theNumberOfWeeks - 2
                                     toDate:self.startDate
                                    options:0];
/*
#warning LOG #1 (START & END Period Date)
 
  //--------------LOG-------------//
  
  NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
  [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
 
  NSLog(@"START period Date: %@", [newDateFormatter stringFromDate:self.startDate]);
  NSLog(@"END period Date: %@", [newDateFormatter stringFromDate:self.endDate]);
  
  //------------------------------//
 */
}

- (void)incrementPeriod
{
/*
#warning LOG #2 (END Period Date BEFORE & AFTER Increment)
  
  //--------------LOG-------------//
  
  NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
  [newDateFormatter setDateFormat:@"yyyy/MM/dd EEEE HH:mm"];
  NSLog(@"End Date Before increment: %@", [newDateFormatter stringFromDate:self.endDate]);
  
  //------------------------------//
*/
  NSCalendar *calendar = [NSCalendar currentCalendar];
  self.endDate = [calendar dateByAddingUnit:NSCalendarUnitWeekOfYear value:1 toDate:self.endDate options:0];
/*
  //--------------LOG-------------//
  
  NSLog(@"End Date After increment: %@", [newDateFormatter stringFromDate:self.endDate]);
  
  //------------------------------//
 */
}

@end
