#import "LQStep.h"

@interface LQStep ()

// Private interface goes here.

@end

@implementation LQStep

- (void)createWithTitle:(NSString *)theTitle creationDate:(NSDate *)theCreationDate andDueDate:(NSDate *)theDate
{
  self.title = theTitle;
  self.dueDate = theDate;
  
  [self scheduleNotification];
  
  if (self.creationDate)
  {
    return;
  }
  else
  {
    self.creationDate = theCreationDate;
  }
}

- (void)toggleChecked
{
  self.checkedValue = !self.checkedValue;
  [self scheduleNotification];
}

- (void)scheduleNotification
{
  UILocalNotification *existingNotification = [self notificationForThisStep];
  
  if (existingNotification) {
/*
#warning Schedule Step Notification
   
    //-----------------------------//
    
    NSLog(@" ");
    NSLog(@"Found an exiting notification %@", existingNotification);
    NSLog(@"And DELETE IT");
    NSLog(@" ");
    
    //-----------------------------//
*/
    
    [[UIApplication sharedApplication]cancelLocalNotification:existingNotification];
  }
  
  if (!self.checkedValue && [self.dueDate compare:[NSDate date]] != NSOrderedAscending)
  {
    UILocalNotification *stepNotification = [[UILocalNotification alloc]init];
    
    stepNotification.fireDate = self.dueDate;
    stepNotification.timeZone = [NSTimeZone defaultTimeZone];
    stepNotification.alertBody = self.title;
    stepNotification.soundName = UILocalNotificationDefaultSoundName;
    stepNotification.userInfo = @{@"NotificationKey": @"Step",
                                  @"StepKey"        : self.title };
    
    [[UIApplication sharedApplication] scheduleLocalNotification:stepNotification];
    
    //-----------------------------//
    
    NSLog(@"Scheduled notification %@ for step title: %@", stepNotification, self.title);
    
    //-----------------------------//
  }
}

- (UILocalNotification *)notificationForThisStep
{
  NSArray *allNotifications = [[UIApplication sharedApplication]scheduledLocalNotifications];
  
  for (UILocalNotification *notification in allNotifications)
  {
    NSString *notificationKey = [notification.userInfo objectForKey:@"StepKey"];
    
    if (notificationKey && [notificationKey isEqualToString:self.title])
    {
      return notification;
    }
  }
  return nil;
}

@end
