#import "_LQWeekDayRemind.h"

@interface LQWeekDayRemind : _LQWeekDayRemind {}

- (void)createWithWeekDay:(NSInteger)theWeekDay andTime:(NSDate *)theTime;

@end
