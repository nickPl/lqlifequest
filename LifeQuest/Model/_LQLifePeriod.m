// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifePeriod.m instead.

#import "_LQLifePeriod.h"

const struct LQLifePeriodAttributes LQLifePeriodAttributes = {
	.endDate = @"endDate",
	.startDate = @"startDate",
};

const struct LQLifePeriodRelationships LQLifePeriodRelationships = {
	.userPlan = @"userPlan",
};

@implementation LQLifePeriodID
@end

@implementation _LQLifePeriod

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LQLifePeriod" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LQLifePeriod";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LQLifePeriod" inManagedObjectContext:moc_];
}

- (LQLifePeriodID*)objectID {
	return (LQLifePeriodID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic endDate;

@dynamic startDate;

@dynamic userPlan;

@end

