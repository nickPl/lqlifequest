// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LQLifePlan.h instead.

#import <CoreData/CoreData.h>

extern const struct LQLifePlanRelationships {
	__unsafe_unretained NSString *currentLongTermQuests;
	__unsafe_unretained NSString *currentWeeklyQuest;
	__unsafe_unretained NSString *dailyReminds;
	__unsafe_unretained NSString *lifeQuests;
	__unsafe_unretained NSString *plannedPeriod;
} LQLifePlanRelationships;

@class LQLongTermLifeQuest;
@class LQLifeQuest;
@class LQWeekDayRemind;
@class LQLifeQuest;
@class LQLifePeriod;

@interface LQLifePlanID : NSManagedObjectID {}
@end

@interface _LQLifePlan : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LQLifePlanID* objectID;

@property (nonatomic, strong) NSSet *currentLongTermQuests;

- (NSMutableSet*)currentLongTermQuestsSet;

@property (nonatomic, strong) LQLifeQuest *currentWeeklyQuest;

//- (BOOL)validateCurrentWeeklyQuest:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *dailyReminds;

- (NSMutableSet*)dailyRemindsSet;

@property (nonatomic, strong) NSSet *lifeQuests;

- (NSMutableSet*)lifeQuestsSet;

@property (nonatomic, strong) LQLifePeriod *plannedPeriod;

//- (BOOL)validatePlannedPeriod:(id*)value_ error:(NSError**)error_;

@end

@interface _LQLifePlan (CurrentLongTermQuestsCoreDataGeneratedAccessors)
- (void)addCurrentLongTermQuests:(NSSet*)value_;
- (void)removeCurrentLongTermQuests:(NSSet*)value_;
- (void)addCurrentLongTermQuestsObject:(LQLongTermLifeQuest*)value_;
- (void)removeCurrentLongTermQuestsObject:(LQLongTermLifeQuest*)value_;

@end

@interface _LQLifePlan (DailyRemindsCoreDataGeneratedAccessors)
- (void)addDailyReminds:(NSSet*)value_;
- (void)removeDailyReminds:(NSSet*)value_;
- (void)addDailyRemindsObject:(LQWeekDayRemind*)value_;
- (void)removeDailyRemindsObject:(LQWeekDayRemind*)value_;

@end

@interface _LQLifePlan (LifeQuestsCoreDataGeneratedAccessors)
- (void)addLifeQuests:(NSSet*)value_;
- (void)removeLifeQuests:(NSSet*)value_;
- (void)addLifeQuestsObject:(LQLifeQuest*)value_;
- (void)removeLifeQuestsObject:(LQLifeQuest*)value_;

@end

@interface _LQLifePlan (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableSet*)primitiveCurrentLongTermQuests;
- (void)setPrimitiveCurrentLongTermQuests:(NSMutableSet*)value;

- (LQLifeQuest*)primitiveCurrentWeeklyQuest;
- (void)setPrimitiveCurrentWeeklyQuest:(LQLifeQuest*)value;

- (NSMutableSet*)primitiveDailyReminds;
- (void)setPrimitiveDailyReminds:(NSMutableSet*)value;

- (NSMutableSet*)primitiveLifeQuests;
- (void)setPrimitiveLifeQuests:(NSMutableSet*)value;

- (LQLifePeriod*)primitivePlannedPeriod;
- (void)setPrimitivePlannedPeriod:(LQLifePeriod*)value;

@end
