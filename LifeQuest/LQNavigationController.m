//
//  LQNavigationController.m
//  LifeQuest
//
//  Created by Plumb on 09.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQNavigationController.h"

@interface LQNavigationController ()

@end

@implementation LQNavigationController

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

@end
