//
//  LQRemindOptionsTableViewController.m
//  LifeQuest
//
//  Created by Plumb on 18.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQRemindOptionsTableViewController.h"
#import "LQLifeQuest.h"
#import "LQWeeklyRemindTableViewController.h"
#import "LQLifePeriod.h"
#import "LQDailyRemindsInfoTableViewController.h"
#import "LQWeekDayRemind.h"
#import "LQLifePlan.h"
#import "LQSharedManager.h"
#import "LQWeeklyQuestViewController.h"
#import "JGProgressHUD.h"
#import "JGProgressHUDSuccessIndicatorView.h"

@interface LQRemindOptionsTableViewController ()

@property (weak, nonatomic) UIBarButtonItem *doneButton;
@property (strong, nonatomic) LQLifePeriod *lifePeriod;
@property (weak, nonatomic) IBOutlet UIImageView *weeklyImage;
@property (weak, nonatomic) IBOutlet UIImageView *dailyImage;
@property (strong, nonatomic) NSMutableArray *dailyRemindsArray;
@property (strong, nonatomic) LQLifePlan *lifePlan;
@property (weak, nonatomic) IBOutlet UITableViewCell *weeklyCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *dailyCell;

@end

@implementation LQRemindOptionsTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
 
  self.navigationItem.title = @"Reminds Options";
  UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
  selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
  
  self.weeklyCell.selectedBackgroundView = selectionView;
  self.dailyCell.selectedBackgroundView = selectionView;
  
  UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"check 2"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(doneAction)];
  doneButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
  
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back Arrow"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(backAction)];
  backButton.tintColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
  
  self.doneButton = doneButton;
  self.navigationItem.rightBarButtonItem = self.doneButton;
  self.navigationItem.leftBarButtonItem = backButton;
  
  [self updateUI];
}

- (void)updateUI
{
  if (self.lifePeriod)
  {
    self.weeklyImage.image = [UIImage imageNamed:@"EditImg"];
    self.doneButton.enabled = YES;
  }
  else
  {
    self.weeklyImage.image = [UIImage imageNamed:@"SetImg"];
    self.doneButton.enabled = NO;
  }
  
  if (self.dailyRemindsArray)
  {
    for (LQWeekDayRemind *remind in self.dailyRemindsArray)
    {
      if (remind.shouldRemindValue)
      {
        self.dailyImage.image = [UIImage imageNamed:@"EditImg"];
        
        return;
      }
    }
  }
  else
  {
    self.dailyImage.image = [UIImage imageNamed:@"SetImg"];
  }
}

#pragma mark - Action

- (void)doneAction
{
  JGProgressHUD *hud = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
  hud.indicatorView = [[JGProgressHUDSuccessIndicatorView alloc]init];
  hud.textLabel.text = @"Let's Start!";
  
  [hud showInView:self.view];
  
  LQLifePlan *plan = [LQLifePlan insertInManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];

  if (!self.dailyRemindsArray)
  {
    [self createRemindersForAllWeekDays];
  }
  
  [plan createWithPlannedPeriod:self.lifePeriod
             selectedLifeQuests:self.allQuestsArray
              andDailyReminders:self.dailyRemindsArray];
  self.lifePlan = plan;
  
  [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"FirstTime"];
  
  [[LQSharedManager sharedManager] saveContext];
  
  [self performSelector:@selector(closeScreen) withObject:nil afterDelay:1.2f];
}

- (void)createRemindersForAllWeekDays
{
  self.dailyRemindsArray = [[NSMutableArray alloc]initWithCapacity:7];
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *dateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute
                                           fromDate:[NSDate date]];
  NSDate *fireTime = [calendar dateFromComponents:dateComp];
  
  for (int i = 1; i <= 7; i++)
  {
    LQWeekDayRemind *dayRemind = [LQWeekDayRemind insertInManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];
    [dayRemind createWithWeekDay:i andTime:fireTime];
    [dayRemind setShouldRemindValue:NO];
    [self.dailyRemindsArray addObject:dayRemind];
    
    [[LQSharedManager sharedManager] saveContext];
  }
}
- (void)closeScreen
{
  [self performSegueWithIdentifier:@"weeklyQuestSegue" sender:nil];
}
- (void)backAction
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (IBAction)weeklyRemindDidSetRemind:(UIStoryboardSegue *)segue
{
  LQWeeklyRemindTableViewController *vc = segue.sourceViewController;
  self.lifePeriod = vc.periodToEdit;
  
  [self updateUI];
}

- (IBAction)dailyRemindDidSetReminds:(UIStoryboardSegue *)segue
{
  LQDailyRemindsInfoTableViewController *vc = segue.sourceViewController;
  self.dailyRemindsArray = vc.weekDaysArray;
  
  [self updateUI];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"weeklyRemindSegue"])
  {
    LQWeeklyRemindTableViewController *vc = (LQWeeklyRemindTableViewController *)segue.destinationViewController;
    vc.periodToEdit = self.lifePeriod;
    vc.numberOfWeeks = self.numberOfWeeks;
  }
  else if ([segue.identifier isEqualToString:@"dailyRemindsInfoSegue"])
  {
    LQDailyRemindsInfoTableViewController *vc = (LQDailyRemindsInfoTableViewController *)segue.destinationViewController;
    
    if (self.dailyRemindsArray)
    {
      vc.weekDaysArray = self.dailyRemindsArray;
    }
  }
  else if ([segue.identifier isEqualToString:@"weeklyQuestSegue"])
  {
    UINavigationController *nc = segue.destinationViewController;
    LQWeeklyQuestViewController *vc = (LQWeeklyQuestViewController *)nc.topViewController;
    vc.lifePlan = self.lifePlan;
  }
}

@end
