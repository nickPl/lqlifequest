//
//  CalendarView.m
//  LifeQuest
//
//  Created by Plumb on 15.03.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQCalendarView.h"
#import "LQCalendarDayView.h"

@interface LQCalendarView ()

@property (weak, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UILabel *dayTitleLabel;

@property (strong, nonatomic) NSDate *todayDate;

@end

@implementation LQCalendarView

#pragma mark - Lifecycle

- (void)awakeFromNib
{
  [self setup];
}

- (void)layoutSubviews
{
  self.dayTitleLabel.text = [self formatDate:self.todayDate];
  [self layoutDaysViews];
}

- (void)setup
{
  self.todayDate = [NSDate date];
}

#pragma mark - Private

- (NSString *)formatDate:(NSDate *)theDate
{
  static NSDateFormatter *formatter = nil;
  if (!formatter) {
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"EEEE, MMM dd"];
  }
  return [formatter stringFromDate:theDate];
}

- (CGSize)dayViewSize
{
  CGSize windowSize = self.bounds.size;
  CGFloat numberOfDaysPerWeek = 7.0;
  CGFloat width = windowSize.width/numberOfDaysPerWeek;
  CGFloat height = 70.0f;
  
  return CGSizeMake(width, height);
}

- (void)layoutDaysViews
{
  CGFloat yOffset = 0;
  CGFloat headerOffset = self.header.frame.size.height;
  
  CGFloat width = self.dayViewSize.width;
  CGFloat height = self.dayViewSize.height;
  
  NSUInteger columnCount = 7;

  for (NSUInteger column = 0; column < columnCount; column++)
  {
    CGRect frame = CGRectMake(column*width, yOffset + headerOffset, width, height);
    LQCalendarDayView *dayView = [[LQCalendarDayView alloc]initFithFrame:frame andIndexNumber:column];
    [self addSubview:dayView];
  }
}

@end
