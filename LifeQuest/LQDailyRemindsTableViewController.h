//
//  LQDailyRemindsTableViewController.h
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LQDailyRemindsTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *weekDaysArray;

@end
