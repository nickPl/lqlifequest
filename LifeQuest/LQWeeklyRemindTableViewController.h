//
//  LQWeeklyRemindTableViewController.h
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLifePeriod;

@interface LQWeeklyRemindTableViewController : UITableViewController

@property (strong, nonatomic) LQLifePeriod *periodToEdit;
@property (assign, nonatomic) NSInteger numberOfWeeks;

@end
