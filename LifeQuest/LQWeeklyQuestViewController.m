//
//  LQWeeklyQuestViewController.m
//  LifeQuest
//
//  Created by Plumb on 19.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQWeeklyQuestViewController.h"
#import "LQQuestDetailTableViewController.h"
#import "LQLifePlan.h"
#import "LQLifeQuest.h"
#import "LQLongTermLifeQuest.h"
#import "LQWeeklyQuestNoteTableViewController.h"
#import "LQLongTermQuestDetailCDTVC.h"
#import "LQRemindOptionsTableViewController.h"
#import "LQDailyRemindsInfoTableViewController.h"
#import "LQSharedManager.h"
#import <MRProgress/MRProgress.h>
#import "LQCalendarView.h"

@interface LQWeeklyQuestViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *sortedLongTermQuests;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet LQCalendarView *calendarView;

@end

@implementation LQWeeklyQuestViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [[NSNotificationCenter defaultCenter]addObserver:self
                                          selector:@selector(handleDataModelChange)
                                              name:NSManagedObjectContextDidSaveNotification
                                            object:nil];
  
  
  /*
  LQLongTermLifeQuest *longTermQuest = [LQLongTermLifeQuest insertInManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];
  [longTermQuest createWithTitle:[NSString stringWithFormat:@"Long-Term 1"]];
  
  [self.lifePlan addCurrentLongTermQuestsObject:longTermQuest];
  [[LQSharedManager sharedManager] saveContext];
  */
  
  [self sortLongTermQuests];
}

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)handleDataModelChange
{
  [self sortedLongTermQuests];
  [self.tableView reloadData];
}

- (void)sortLongTermQuests
{
  if ([self.lifePlan.currentLongTermQuests count] > 0)
  {
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
    NSArray *sortedLongTermQuests = [[self.lifePlan.currentLongTermQuests allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    self.sortedLongTermQuests = [NSMutableArray arrayWithArray:sortedLongTermQuests];
  }
}

- (NSMutableArray *)sortDailyReminds
{
  NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"weekDay" ascending:YES]];
  NSMutableArray *dailyReminds = [NSMutableArray arrayWithArray:[[self.lifePlan.dailyReminds allObjects] sortedArrayUsingDescriptors:sortDescriptors]];
  return dailyReminds;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  NSUInteger numberOfLongTermQuest = [self.sortedLongTermQuests count] == 0 ? 1 : [self.sortedLongTermQuests count];
  return section == 0 ? 2 : numberOfLongTermQuest;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell * cell = nil;
  
  if (indexPath.section == 0)
  {
    if (indexPath.row == 0)
    {
      cell = [tableView dequeueReusableCellWithIdentifier:@"weeklyQuestCell"];
      
      UILabel *weeklyQuestTitleLabel = (UILabel *)[cell viewWithTag:1001];
      weeklyQuestTitleLabel.text = self.lifePlan.currentWeeklyQuest.title;
      
      if (self.lifePlan.currentWeeklyQuest.note)
      {
        UILabel *weeklyQuestNoteLabel = (UILabel *)[cell viewWithTag:1002];
        weeklyQuestNoteLabel.text = self.lifePlan.currentWeeklyQuest.note;
        weeklyQuestNoteLabel.hidden = NO;
        
        UILabel *weeklyQuestNoteTitleLabel = (UILabel *)[cell viewWithTag:1003];
        weeklyQuestNoteTitleLabel.hidden = NO;
      }
      else
      {
        UILabel *weeklyQuestNoteLabel = (UILabel *)[cell viewWithTag:1002];
        weeklyQuestNoteLabel.hidden = YES;
        
        UILabel *weeklyQuestNoteTitleLabel = (UILabel *)[cell viewWithTag:1003];
        weeklyQuestNoteTitleLabel.hidden = YES;
      }
    }
    else if (indexPath.row == 1)
    {
      cell = [tableView dequeueReusableCellWithIdentifier:@"addNoteCell"];
      
      UILabel *addNoteLabel = (UILabel *)[cell viewWithTag:1004];
      addNoteLabel.text = self.lifePlan.currentWeeklyQuest.note ? @"EDIT NOTE" : @"ADD NOTE";
      
      UIView *selectionView = [[UIView alloc]initWithFrame:CGRectZero];
      selectionView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
      
      cell.selectedBackgroundView = selectionView;
    }
  }
  else if (indexPath.section == 1)
  {
    cell = [tableView dequeueReusableCellWithIdentifier:@"longTermQuestCell"];
    
    if ([self.sortedLongTermQuests count] == 0)
    {
      UILabel *longTermQuestTitleLabel = (UILabel *)[cell viewWithTag:1005];
      longTermQuestTitleLabel.text = @"No Long-Term Quest";
      
      UIButton *longTermQuestDetailButton= (UIButton *)[cell viewWithTag:1006];
      longTermQuestDetailButton.hidden = YES;
      
      MRCircularProgressView *longTermQuestProgressIndicator = (MRCircularProgressView *)[cell viewWithTag:1007];
      longTermQuestProgressIndicator.hidden = YES;
    }
    else
    {
      [self configureCell:cell atIndexPath:indexPath];
    }
  }
  return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete)
  {
    LQLongTermLifeQuest *quest = self.sortedLongTermQuests[indexPath.row];
    [self.sortedLongTermQuests removeObject:quest];
    [self.lifePlan removeCurrentLongTermQuestsObject:quest];
    
    [[LQSharedManager sharedManager].managedObjectContext deleteObject:quest];
    
    if ([self.sortedLongTermQuests count] == 0)
    {
      [self.tableView beginUpdates];
      [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
      [self.tableView endUpdates];
    }
    else
    {
      [self.tableView beginUpdates];
      [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
      [self.tableView endUpdates];
    }
    
    [[LQSharedManager sharedManager] saveContext];
  }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
  LQLongTermLifeQuest *quest = self.sortedLongTermQuests[indexPath.row];
  
  UILabel *longTermQuestTitleLabel = (UILabel *)[cell viewWithTag:1005];
  longTermQuestTitleLabel.text = quest.title;
  
  MRCircularProgressView *longTermQuestProgressIndicator = (MRCircularProgressView *)[cell viewWithTag:1007];
  longTermQuestProgressIndicator.borderWidth = 2;
  longTermQuestProgressIndicator.lineWidth = 4;
  longTermQuestProgressIndicator.animationDuration = 0.8;
  
  float progress = quest.progressValue == 0 ? 0 : (float)quest.progressValue / 100;
  [longTermQuestProgressIndicator setProgress:progress];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.section == 1 || indexPath.row == 0)
  {
    return nil;
  }
  else
  {
    return indexPath;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.row == 0 || indexPath.section == 1)
  {
    return 88;
  }
  else
  {
    return 44;
  }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.section == 1 && [self.sortedLongTermQuests count] != 0)
  {
    return UITableViewCellEditingStyleDelete;
  }
  else
  {
    return UITableViewCellEditingStyleNone;
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return section == 0 ? 0.1 : 20.f;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"addQuestToLifePlanSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQQuestDetailTableViewController *tvc = (LQQuestDetailTableViewController *)nc.topViewController;
    tvc.lifePlan = self.lifePlan;
  }
  else if ([segue.identifier isEqualToString:@"addNoteSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQWeeklyQuestNoteTableViewController *tvc = (LQWeeklyQuestNoteTableViewController *)nc.topViewController;
    tvc.quest = self.lifePlan.currentWeeklyQuest;
  }
  else if ([segue.identifier isEqualToString:@"longTermQuestDetailSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQLongTermQuestDetailCDTVC *tvc = (LQLongTermQuestDetailCDTVC *)nc.topViewController;
    
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:sender];
    tvc.currentLongTermQuest = self.sortedLongTermQuests[cellIndexPath.row];
  }
  else if ([segue.identifier isEqualToString:@"remindsSettingsSegue"])
  {
    UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
    LQDailyRemindsInfoTableViewController *tvc = (LQDailyRemindsInfoTableViewController *)nc.topViewController;
    tvc.weekDaysArray = [self sortDailyReminds];
    tvc.lifePlan = self.lifePlan;
  }
}

@end
