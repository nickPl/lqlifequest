//
//  StepDetailViewController.m
//  LifeQuest
//
//  Created by Plumb on 26.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import "LQStepDetailViewController.h"
#import "LQStep.h"
#import "LQSharedManager.h"
#import "LQLongTermLifeQuest.h"

@interface LQStepDetailViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *stepTitleTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation LQStepDetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  if (!self.stepToEdit)
  {
    [self.stepTitleTextField becomeFirstResponder];
    self.saveButton.enabled = NO;
  }
  else
  {
    self.stepTitleTextField.text = self.stepToEdit.title;
    self.datePicker.date = self.stepToEdit.dueDate;
  }
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender
{
  LQStep *step = nil;
  
  if (self.stepToEdit)
  {
    step = self.stepToEdit;
  }
  else
  {
    step = [LQStep insertInManagedObjectContext:[[LQSharedManager sharedManager] managedObjectContext]];
    [self.quest addStep:step];
  }
  [step createWithTitle:self.stepTitleTextField.text creationDate:[NSDate date] andDueDate:self.datePicker.date];
  
  [[LQSharedManager sharedManager] saveContext];
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
  
  self.saveButton.enabled = ([newText length] > 0);
  
  return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if ([textField isEqual:self.stepTitleTextField])
  {
    [self.stepTitleTextField resignFirstResponder];
  }
  
  return NO;
}

@end
