//
//  StepDetailViewController.h
//  LifeQuest
//
//  Created by Plumb on 26.02.15.
//  Copyright (c) 2015 Nikolas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LQLongTermLifeQuest;
@class LQStep;

@interface LQStepDetailViewController : UIViewController

@property (strong, nonatomic) LQLongTermLifeQuest *quest;
@property (strong, nonatomic) LQStep *stepToEdit;

@end
